<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemsPhotosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemsPhotosTable Test Case
 */
class ItemsPhotosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemsPhotosTable
     */
    public $ItemsPhotos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.items_photos',
        'app.items',
        'app.materials',
        'app.items_materials',
        'app.photos',
        'app.tags',
        'app.items_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemsPhotos') ? [] : ['className' => ItemsPhotosTable::class];
        $this->ItemsPhotos = TableRegistry::get('ItemsPhotos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemsPhotos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
