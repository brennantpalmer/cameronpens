<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemsMaterialsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemsMaterialsTable Test Case
 */
class ItemsMaterialsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemsMaterialsTable
     */
    public $ItemsMaterials;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.items_materials',
        'app.items',
        'app.materials',
        'app.photos',
        'app.items_photos',
        'app.tags',
        'app.items_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemsMaterials') ? [] : ['className' => ItemsMaterialsTable::class];
        $this->ItemsMaterials = TableRegistry::get('ItemsMaterials', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemsMaterials);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
