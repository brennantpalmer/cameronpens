<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemsCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemsCategoriesTable Test Case
 */
class ItemsCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemsCategoriesTable
     */
    public $ItemsCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.items_categories',
        'app.items',
        'app.materials',
        'app.items_materials',
        'app.photos',
        'app.items_photos',
        'app.tags',
        'app.items_tags',
        'app.categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemsCategories') ? [] : ['className' => ItemsCategoriesTable::class];
        $this->ItemsCategories = TableRegistry::get('ItemsCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemsCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
