<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrdersOrderitemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrdersOrderitemsTable Test Case
 */
class OrdersOrderitemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrdersOrderitemsTable
     */
    public $OrdersOrderitems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.orders_orderitems',
        'app.orders',
        'app.orderitems'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrdersOrderitems') ? [] : ['className' => OrdersOrderitemsTable::class];
        $this->OrdersOrderitems = TableRegistry::get('OrdersOrderitems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrdersOrderitems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
