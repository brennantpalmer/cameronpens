<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
/**
 * Communications Controller
 *
 * @property \App\Model\Table\CommunicationsTable $Communications
 *
 * @method \App\Model\Entity\Communication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommunicationsController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow([
            'add',
            'success'
            ]);
    }

    public function beforeFilter(Event $event)
	{
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('communications_layout');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('admin_layout');        

        $communications = $this->paginate($this->Communications);

        $this->set(compact('communications'));
    }

    /**
     * View method
     *
     * @param string|null $id Communication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('admin_layout');        
     
        $communication = $this->Communications->get($id, [
            'contain' => []
        ]);

        $this->set('communication', $communication);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $communication = $this->Communications->newEntity();
        if ($this->request->is('post')) {
            $communication = $this->Communications->patchEntity($communication, $this->request->getData());
            if ($this->Communications->save($communication)) {
                
                $successmessage = "The message has been sent.";
                $this->set('successmessage', $successmessage);
                $this->set(compact('communication'));

                $email = new Email('default');
                $email->from(['noreply@brennantpalmer.com' => 'brennantpalmer.com'])
                    ->to($communication->email)
                    ->subject('We have received your message')
                    ->send(
                        'Thank you for contacting us! You\'re message has been received and someone will contact you shortly.
                        
                        Thank you,
                        cameronpens.com
                        THIS IS AN AUTOMATED MESSAGE.
                        '
                    );

                return $this->redirect(['action' => 'success']);
            }
            else
            {
                $successmessage = "The communication could not be saved. Please, try again.";
                $this->set('successmessage', $successmessage);  
                $this->set(compact('communication'));
                
            }
        }
        else
        {
            $this->set(compact('communication'));
            $successmessage = "";
                $this->set('successmessage', $successmessage);        
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Communication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('admin_layout');        

        $communication = $this->Communications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $communication = $this->Communications->patchEntity($communication, $this->request->getData());
            if ($this->Communications->save($communication)) {
                $this->Flash->success(__('The communication has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The communication could not be saved. Please, try again.'));
        }
        $this->set(compact('communication'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Communication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $communication = $this->Communications->get($id);
        if ($this->Communications->delete($communication)) {
            $this->Flash->success(__('The communication has been deleted.'));
        } else {
            $this->Flash->error(__('The communication could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function success()
    {
       
    }
}
