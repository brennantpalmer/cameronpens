<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ItemsCategories Controller
 *
 * @property \App\Model\Table\ItemsCategoriesTable $ItemsCategories
 *
 * @method \App\Model\Entity\ItemsCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemsCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Items', 'Categories']
        ];
        $itemsCategories = $this->paginate($this->ItemsCategories);

        $this->set(compact('itemsCategories'));
    }

    /**
     * View method
     *
     * @param string|null $id Items Category id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemsCategory = $this->ItemsCategories->get($id, [
            'contain' => ['Items', 'Categories']
        ]);

        $this->set('itemsCategory', $itemsCategory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $itemsCategory = $this->ItemsCategories->newEntity();
        if ($this->request->is('post')) {
            $itemsCategory = $this->ItemsCategories->patchEntity($itemsCategory, $this->request->getData());
            if ($this->ItemsCategories->save($itemsCategory)) {
                $this->Flash->success(__('The items category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The items category could not be saved. Please, try again.'));
        }
        $items = $this->ItemsCategories->Items->find('list', ['limit' => 200]);
        $categories = $this->ItemsCategories->Categories->find('list', ['limit' => 200]);
        $this->set(compact('itemsCategory', 'items', 'categories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Items Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemsCategory = $this->ItemsCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemsCategory = $this->ItemsCategories->patchEntity($itemsCategory, $this->request->getData());
            if ($this->ItemsCategories->save($itemsCategory)) {
                $this->Flash->success(__('The items category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The items category could not be saved. Please, try again.'));
        }
        $items = $this->ItemsCategories->Items->find('list', ['limit' => 200]);
        $categories = $this->ItemsCategories->Categories->find('list', ['limit' => 200]);
        $this->set(compact('itemsCategory', 'items', 'categories'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Items Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $itemsCategory = $this->ItemsCategories->get($id);
        if ($this->ItemsCategories->delete($itemsCategory)) {
            $this->Flash->success(__('The items category has been deleted.'));
        } else {
            $this->Flash->error(__('The items category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
