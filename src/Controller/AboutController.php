<?php
namespace App\Controller;

use App\Controller\AppController;

class AboutController extends AppController
{

    ////////////////////////////////////////////////////////////////////////////////
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['display', 'view', 'index']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('communications_layout');
    }
    
}
