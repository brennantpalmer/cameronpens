<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 *
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemsController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Search.Prg', [
            // This is default config. You can modify "actions" as needed to make
            // the PRG component work only for specified methods.
            'actions' => ['index']
        ]);

        $this->loadComponent('MaterialsFilter');

        $this->Auth->allow([
            'display', 
            'view', 
            'index',
            'featured',
            'onsale',
            'favorite',
            'hot',
            'bundles',
            'addToCart',
            'removeFromCart',
            'cart',
            'getCartCount',
            'destroyCart',
            'checkifincart',
            'updateCart',
            'setshippingstandard',
            'setshippingexpress',
            'setshippingtwoday',
            'testshipping'
            ]);
    }
    ////////////////////////////////////////////////////////////////////////////////

    public function beforeFilter(Event $event)
	{
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('items_layout');
    }
////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////
    //////////////////////*** START ITEM CRUD FUNCTIONS ***//////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        
        $adminLoggedIn = $this->Auth->user();
        $desiredMaterials = $this->request->getQueryParams();

        if(!isset($desiredMaterials)){
            $desiredMaterials = array();
        }

        $paramArray = $this->MaterialsFilter->createFilter($desiredMaterials);

        
        if(empty($paramArray)){
            array_push($paramArray, ['active' => true]);
        }

        if(!$adminLoggedIn){

            $query = $this->Items
            // Use the plugins 'search' custom finder and pass in the
            // processed query params
            ->find('search', ['search' => $this->request->getQueryParams()])
            // You can add extra things to the query if you need to
            ->where([['AND' => ['active' => true]],'OR' => $paramArray])
            ->contain(['Photos', 'Materials', 'Categories', 'Tags']);
            
        }
        else
        {
            array_push($paramArray, ['active' => false]);

            $query = $this->Items
            // Use the plugins 'search' custom finder and pass in the
            // processed query params
            ->find('search', ['search' => $this->request->getQueryParams()])
            // You can add extra things to the query if you need to
            ->where(['OR' => $paramArray])
            ->contain(['Photos', 'Materials', 'Categories', 'Tags']);
            

        }

        
        
        
        $this->set('items', $this->paginate($query));
        $this->set('queryParams', $this->request->getQueryParams());      

    }

    /**
     * View method
     *
     * @param string|null $id Item id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('item_layout');

        if($id == null){
            $this->Flash->error(__('The item could not be found.'));
            return $this->redirect(['action' => 'index']);
        }

        $ItemsTable = TableRegistry::get('Items');
        $exists = $ItemsTable->exists(['id' => $id]);

        if (!$exists) {
            $this->Flash->error(__('The item could not be found.'));
            return $this->redirect(['action' => 'index']);
        }

        $itemsyoumaylike = $this->Items->find('all')->order('rand()');
        $itemsyoumaylike->contain(['Photos']);

        $itemsrecentlyviewed = $this->Items->find('all')->order('rand()');
        $itemsrecentlyviewed->contain(['Photos']);
        
        $item = $this->Items->get($id, [
            'contain' => ['Materials', 'Photos', 'Tags','Categories']
        ]);

        // Calcualte the percentage off for display
        if($item->onsale)
        {
            $percentageoff = ($item->standardprice - $item->saleprice)/$item->standardprice;
            $percentageoff *= 100;
            $percentageoff = round($percentageoff, 0, PHP_ROUND_HALF_DOWN);
            if($percentageoff === 100.)
            {
                $percentageoff = 99;
            }
            $this->set('percentageoff', $percentageoff);
        }

        
        $this->set('itemsyoumaylike',$itemsyoumaylike);
        $this->set('itemsrecentlyviewed',$itemsrecentlyviewed);
        $this->set('item', $item);
        $this->set('isincart', $this->checkifincart($item->id));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('admin_layout');        

        $item = $this->Items->newEntity();
        if ($this->request->is('post')) {

            $data = $this->request->getData();
            $item = $this->Items->patchEntity($item, $data);

            
            if($item->onsale){
                $item->actualprice = $item->saleprice;
            }else{
                $item->actualprice = $item->standardprice;
            }
            
            if ($this->Items->save($item)) 
            {
                
                $submittedPictureData = $data['pictures'];

                if($submittedPictureData)
                {
                    $PhotosTable = TableRegistry::get('Photos');                                        

                    foreach($submittedPictureData as $data)
                    {              
                            $photo = $PhotosTable->newEntity();
                            $tempArray = array();
                            $tempArray['items']['_ids'][0] = $item->id;
                            $tempArray['title'] = $data;
                            $tempArray['title']['name'] = rand() . $tempArray['title']['name'];
                            $PhotosTable->patchEntity($photo,$tempArray);

                            if(!$PhotosTable->save($photo))
                            {
                                $this->Flash->error(__('The picture could not be saved.'));
                            }
                            else
                            {
                                $this->Flash->success(__('The picture has been saved: ' . $photo->title));
                            }                     
                    }
                }
            
                $this->Flash->success(__('The item has been saved: ' . $item->title));
                return $this->redirect(['action' => 'add']);
            }
            else
            {
                $this->Flash->error(__('The item could not be saved. Please, try again.'));            
            }
        }
        $materials = $this->Items->Materials->find('list', ['limit' => 200]);
        $photos = $this->Items->Photos->find('list', ['limit' => 200]);
        $tags = $this->Items->Tags->find('list', ['limit' => 200]);
        $categories = $this->Items->Categories->find('list', ['limit' => 200]);
        $this->set(compact('item', 'materials', 'photos', 'tags', 'categories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Item id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('item_layout');

        if($id == null){
            $this->Flash->error(__('The item could not be found. the id was null'));
            return $this->redirect(['action' => 'index']);
        }

        $ItemsTable = TableRegistry::get('Items');
        $exists = $ItemsTable->exists(['id' => $id]);

        if (!$exists) {
            $this->Flash->error(__('The item could not be found.'));
            return $this->redirect(['action' => 'index']);
        }

        $item = $this->Items->get($id, [
            'contain' => ['Materials', 'Photos', 'Tags']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $item = $this->Items->patchEntity($item, $this->request->getData());

            if($item->onsale){
                $item->actualprice = $item->saleprice;
            }else{
                $item->actualprice = $item->standardprice;
            }

            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item could not be saved. Please, try again.'));
        }
        $materials = $this->Items->Materials->find('list', ['limit' => 200]);
        $photos = $this->Items->Photos->find('list', ['limit' => 200]);
        $tags = $this->Items->Tags->find('list', ['limit' => 200]);
        $categories = $this->Items->Categories->find('list', ['limit' => 200]);
        $this->set(compact('item', 'materials', 'photos', 'tags', 'categories'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Item id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        if($id == null){
            $this->Flash->error(__('The item could not be found. id was null'));
            return $this->redirect(['action' => 'index']);
        }

        $ItemsTable = TableRegistry::get('Items');
        $exists = $ItemsTable->exists(['id' => $id]);

        if (!$exists) {
            $this->Flash->error(__('The item could not be found.'));
            return $this->redirect(['action' => 'index']);
        }

        $item = $this->Items->get($id);
        if ($this->Items->delete($item)) {
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function featured()
    {
        $this->viewBuilder()->setLayout('items_pages_layout');
        
        $items = $this->Items->find()->where(['featured' => true]);
        $items->contain(['Photos']);
        $items->contain(['Materials']);
        $items->contain(['Categories']);
        $items->contain(['Tags']);
        $this->paginate($items);
        $this->set(compact('items'));
        $this->set('queryParams', $this->request->getQueryParams());      
        
    }

    public function onsale()
    {
        $this->viewBuilder()->setLayout('items_pages_layout');
        
        $items = $this->Items->find()->where(['onsale' => true]);
        $items->contain(['Photos']);
        $items->contain(['Materials']);
        $items->contain(['Categories']);
        $items->contain(['Tags']);
        $this->paginate($items);
        $this->set(compact('items'));
        $this->set('queryParams', $this->request->getQueryParams());      
        
    }

    public function favorite()
    {
        $this->viewBuilder()->setLayout('items_pages_layout');
        
        $items = $this->Items->find()->where(['favorite' => true]);
        $items->contain(['Photos']);
        $items->contain(['Materials']);
        $items->contain(['Categories']);
        $items->contain(['Tags']);
        $this->paginate($items);
        $this->set(compact('items'));
        $this->set('queryParams', $this->request->getQueryParams());      
        
    }

    public function hot()
    {
        $this->viewBuilder()->setLayout('items_pages_layout');
        
        $items = $this->Items->find()->where(['hot' => true]);
        $items->contain(['Photos']);
        $items->contain(['Materials']);
        $items->contain(['Categories']);
        $items->contain(['Tags']);
        $this->paginate($items);
        $this->set(compact('items'));
        $this->set('queryParams', $this->request->getQueryParams());      
        
    }

    public function bundles()
    {
        $this->viewBuilder()->setLayout('items_pages_layout');
        
        $items = $this->Items->find()->where(['bundle' => true]);
        $items->contain(['Photos']);
        $items->contain(['Materials']);
        $items->contain(['Categories']);
        $items->contain(['Tags']);
        $this->paginate($items);
        $this->set(compact('items'));
        $this->set('queryParams', $this->request->getQueryParams());      
        
    }

    ////////////////////////////////////////////////////////////////////////
    //////////////////////*** END ITEM CRUD FUNCTIONS ***///////////////////
    ////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    //////////////////////*** START CART FUNCTIONS ***//////////////////////
    ////////////////////////////////////////////////////////////////////////

    public function addToCart($productID = null)
    {

        $this->autoRender = false;
        if ($this->request->is('post')) {
            if($this->Cart->getCount() < 20000)
            {
                $this->Cart->add($productID);
            }
            
            //$this->Flash->error(__('$this->request->is(post)'));
        }
        //echo $this->Cart->getCount();
        //$this->Flash->error(__('addToCart($productID = null)'));
        echo $this->getCartCount();
    }

    public function removeFromCart($productID = null)
    {
        $this->Cart->remove($productID);
        return $this->redirect(['action' => 'cart']);
    }

    public function cart()
    {
        $ConfigsTable = TableRegistry::get('Configs');
         
        $config = $ConfigsTable->find('all')->where(['Id' => '1']);
        $taxrate = 0;
        
        foreach($config as $c)
        {
            $taxrate = $c->tax;
        }

        $this->viewBuilder()->setLayout('default');

        $items = array();
        $cart = $this->Cart->readCart();
        
        if($cart)
        {
            foreach($cart as $cartItem)
            {
                $item = $this->Items->get($cartItem['productId'], ['contain' =>['Photos']]);
                
                array_push($items, $item);
            }
        }

        $Cart = $this->Cart->readCartVerbose();
        $this->set('Cart', $Cart);

        $cartTotals = $this->Cart->getcarttotals();
        $this->set('cartTotals', $cartTotals);
        
        $shippingselection = $this->Cart->getshippingselection();
        $this->set('shippingselection', $shippingselection);

        $this->set(compact('items'));
    }


    public function getCartCount()
    {
        return $this->Cart->getCount();
    }

    public function destroyCart()
    {
        $this->autoRender = false;
        $this->Cart->destroyCart();
    }

    public function checkifincart($id = null)
    {
        if($id == null){return false;}
        if($this->Cart->checkifincart($id))
        {
            return true;
        }
        return false;
    }

    public function updatecart()
    {
        $this->autoRender = false;

        $data = $this->request->data;
        if(!$data){return $this->redirect(['action' => 'cart']);}
        foreach($data as $key => $value)
        {
            if(strlen($value) < 4)
            {
                $this->Cart->update($key, $value);                
            }
            else
            {
                $this->Flash->error(__('You cannot add that many items to the cart'));
            }
        }
        return $this->redirect(['action' => 'cart']);
    }
    /////////////////////////////////////////////////////////////////////////////
    //////////////////////*** END CART FUNCTIONS   ***///////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////
    //////////////////////*** BEGIN SHIPPING FUNCTIONS   ***/////////////////////
    /////////////////////////////////////////////////////////////////////////////

    public function setshippingstandard()
    {
        $this->autoRender = false;
        $this->Shipping->setshippingstandard();
        return $this->redirect(['action' => 'cart']);
    }
    
    public function setshippingexpress()
    {
        $this->autoRender = false;
        $this->Shipping->setshippingexpress();
        return $this->redirect(['action' => 'cart']);
        
    }
    
    public function setshippingtwoday()
    {
        $this->autoRender = false;
        $this->Shipping->setshippingtwoday();
        return $this->redirect(['action' => 'cart']);
        
    }


    ////////////////////////////////////////////////////////////////////////////
    //////////////////////*** END SHIPPING FUNCTIONS   ***//////////////////////
    ////////////////////////////////////////////////////////////////////////////


    
}
