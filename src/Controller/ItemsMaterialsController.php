<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ItemsMaterials Controller
 *
 * @property \App\Model\Table\ItemsMaterialsTable $ItemsMaterials
 *
 * @method \App\Model\Entity\ItemsMaterial[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemsMaterialsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Items', 'Materials']
        ];
        $itemsMaterials = $this->paginate($this->ItemsMaterials);

        $this->set(compact('itemsMaterials'));
    }

    /**
     * View method
     *
     * @param string|null $id Items Material id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemsMaterial = $this->ItemsMaterials->get($id, [
            'contain' => ['Items', 'Materials']
        ]);

        $this->set('itemsMaterial', $itemsMaterial);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $itemsMaterial = $this->ItemsMaterials->newEntity();
        if ($this->request->is('post')) {
            $itemsMaterial = $this->ItemsMaterials->patchEntity($itemsMaterial, $this->request->getData());
            if ($this->ItemsMaterials->save($itemsMaterial)) {
                $this->Flash->success(__('The items material has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The items material could not be saved. Please, try again.'));
        }
        $items = $this->ItemsMaterials->Items->find('list', ['limit' => 200]);
        $materials = $this->ItemsMaterials->Materials->find('list', ['limit' => 200]);
        $this->set(compact('itemsMaterial', 'items', 'materials'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Items Material id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemsMaterial = $this->ItemsMaterials->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemsMaterial = $this->ItemsMaterials->patchEntity($itemsMaterial, $this->request->getData());
            if ($this->ItemsMaterials->save($itemsMaterial)) {
                $this->Flash->success(__('The items material has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The items material could not be saved. Please, try again.'));
        }
        $items = $this->ItemsMaterials->Items->find('list', ['limit' => 200]);
        $materials = $this->ItemsMaterials->Materials->find('list', ['limit' => 200]);
        $this->set(compact('itemsMaterial', 'items', 'materials'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Items Material id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $itemsMaterial = $this->ItemsMaterials->get($id);
        if ($this->ItemsMaterials->delete($itemsMaterial)) {
            $this->Flash->success(__('The items material has been deleted.'));
        } else {
            $this->Flash->error(__('The items material could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
