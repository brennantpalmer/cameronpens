<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\ORM\ResultSet;
use Cake\Collection\Collection;



class MaterialsFilterComponent extends Component
{

    public $firstItemID;
    public $theRestOfTheIDs;

    public function createFilter($desiredMaterials)
    {
        if(count($desiredMaterials) < 1){
            return array();
        }

        $MaterialsTable = TableRegistry::get('Materials');
        $materials = array();
        $index = 0;
        $firstMaterial = "";

        foreach($desiredMaterials as $material){
            array_push($materials, ['title' => $material]);                
        }

        $query = $MaterialsTable->find('all')->where(['OR' => $materials])->contain(['Items']);
       
        $index = 0;
        $this->firstItemID = "";
        $this->theRestOfTheIDs = array();

        foreach($query as $material){
            
            foreach($material->items as $item){  
               array_push($this->theRestOfTheIDs, ['id' => $item->id]);
        }
        }

        
        return $this->theRestOfTheIDs;
        
    }

}