<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\ExecutePayment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ShippingAddress;

class PaypalComponent extends Component
{
    private $apiContext;
    private $payment;

    public function intitializeAPI()
    {
        $ConfigsTable = TableRegistry::get('Configs');
        $config = $ConfigsTable->find('all')->where(['Id' => '1']);

        $appID = '';
        $secret = '';

        foreach($config as $c)
        {
            $appID = $c->paypalclientid;
            $secret = $c->paypalsecretkey;
        }


        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
              $appID,
              $secret
            )
          );
    }

    public function createpayment($items, $totals, $order)
    {
        try{
        $this->intitializeAPI();
        // Create new payer and method
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // Set redirect urls
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl('http://localhost/orders/transactionapproved')
        ->setCancelUrl('http://localhost/orders/transactioncancelled' . '?cptransactionid=' . $order->cptransactionid);
        
        $tempItemsArray = array();
        foreach($items as $i)
        {
            $item = new Item();
            $item->setName($i->title)
            ->setCurrency('USD')
            ->setQuantity($i->quantity)
            ->setSku($i->itemid) // Similar to `item_number` in Classic API
            ->setPrice($i->price); 

            array_push($tempItemsArray, $item);
        }

        $shipping_address = new ShippingAddress();
        $shipping_address->setCity($order->city);
        $shipping_address->setCountryCode($order->country);
        $shipping_address->setPostalCode($order->zip);
        $shipping_address->setLine1($order->streetaddress);
        $shipping_address->setState($order->state);
        $shipping_address->setRecipientName($order->firstname . ' ' . $order->lastname);

        $itemList = new ItemList();
        $itemList->setItems($tempItemsArray);
        $itemList->setShippingAddress($shipping_address);

        $details = new Details();
        $details->setShipping($order->shipping)
            ->setTax($order->tax)
            ->setSubtotal($order->subtotal);

        

        // Set payment amount
        $amount = new Amount();
        $amount->setCurrency("USD")
        ->setTotal($order->total)
        ->setDetails($details);

        // Set transaction object
        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setDescription("Payment For Items Bought from Cameron Pens (www.cameronpens.com)")
        ->setItemList($itemList)
        ->setInvoiceNumber($order->cptransactionid);

        // Create the full payment object
        $this->payment = new Payment();
        $this->payment->setIntent('sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

        
        }
        catch(\PayPal\Exception\PayPalConnectionException $ex) {
            debug(json_decode($ex->getCode()));
            debug(json_decode($ex->getData()));
            die($ex);
        }
    }

    public function initializepayment()
    {
        // Create payment with valid API context
        try {
            $paymentArray = array();

            $this->payment->create($this->apiContext);
        
            // Get PayPal redirect URL and redirect user
            $approvalUrl = $this->payment->getApprovalLink();
        
            $paymentArray['url'] = $approvalUrl  . '&useraction=commit';
            $paymentArray['paymentid'] = $this->payment->id;
            return $paymentArray;
            // REDIRECT USER TO $approvalUrl
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            debug(json_decode($ex->getCode()));
            debug(json_decode($ex->getData()));
            throw new \Cake\Core\Exception\Exception(__('PayPal has encountered an error:' . ' |Code:' . $ex->getCode() . ' |Message: ' .  $ex->getData()));
            die($ex);
        } catch (Exception $ex) {
            die($ex);
        }
    }

    public function checkBeforeExec()
    {
        if(!isset($_GET['paymentId'])){return false;}
        if(!isset($_GET['PayerID'])){return false;}

        $paymentId = $_GET['paymentId'];
        $payerId = $_GET['PayerID'];
        
        $OrdersTable = TableRegistry::get('Orders');
        $query = $OrdersTable->find('all')->where(['paypalpaymentid' => $paymentId]);        
        $order = $query->first();

        if($order === null){return false;}
        if($order->approved == true){return false;}
        if($order->cancelled == true){return false;}
        
        return true;
    }

    public function execute()
    {
        $this->intitializeAPI();
        try {

            $result = array();
            // Get payment object by passing paymentId
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $this->apiContext);
            $payerId = $_GET['PayerID'];

            // Execute payment with payer id
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            
            // Execute payment
            $result['result'] = $payment->execute($execution, $this->apiContext);
            $result['paymentid'] = $paymentId;
            $result['payerid'] = $payerId;

            $transactions = $payment->getTransactions();
            $relatedResources = $transactions[0]->getRelatedResources();
            $sale = $relatedResources[0]->getSale();
            $saleId = $sale->getId();
            $result['transactionid'] = $saleId;

            // Return the result
            return $result;

            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                debug(json_decode($ex->getCode()));
                debug($ex->getData());
            die($ex);
            } 
    }
}