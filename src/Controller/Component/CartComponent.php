<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class CartComponent extends Component
{
    /////////////////////////////////////// TOTALS /////////////////////////////////////////////

    public function getcarttotals()
    {
        $cart = $this->readCart();
        $ItemsTable = TableRegistry::get('Items');

        $standardTotal = 0;
        $discountTotal = 0;
        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        $total = 0;
        $cartTotals = array();

        if($cart)
        {
            foreach($cart as $cartItem)
            {
                $itemTotal = 0;
                $item = $ItemsTable->get($cartItem['productId']);


                $itemTotal = $item->actualprice * $cartItem['quantity'];
                $itemstandardtotal = $item->standardprice * $cartItem['quantity'];
                $discount = ($item->standardprice - $item->actualprice) * $cartItem['quantity'];
                
                $standardTotal += $itemstandardtotal;
                $subtotal += $itemTotal;
                $discountTotal += $discount;

            }
        }

        $shipping = $this->getshippingcost();
        $tax = number_format((float)($subtotal * $this->gettax()), 2, '.', '');
        $total = number_format((float)($subtotal + $tax + $shipping), 2, '.', '');
        $discountTotal = number_format((float)($discountTotal), 2, '.', '');
        $standardTotal = number_format((float)($standardTotal), 2, '.', '');
        $subtotal = number_format((float)($subtotal), 2, '.', '');

        $cartTotals['standardTotal'] = $standardTotal;
        $cartTotals['subtotal'] = $subtotal;
        $cartTotals['discountTotal'] = $discountTotal;
        $cartTotals['shipping'] = $shipping;
        $cartTotals['tax'] = $tax;
        $cartTotals['total'] = $total;
            

        return $cartTotals;
    }

    public function gettax()
    {
        $ConfigsTable = TableRegistry::get('Configs');
         
        $config = $ConfigsTable->find('all')->where(['Id' => '1']);
        $tax = 0;
        
        foreach($config as $c)
        {
            $tax = $c->tax;
        }

        return $tax;
    }


    //////////////////////////////////// END TOTALS ////////////////////////////////////////////
    /*
     * add a product to cart
     */
    public function add($productId) {
        $session = $this->request->session();
        $cart = $session->read('Cart');
        
        if ($cart == null){
            $session->write("Cart." . $productId, [
                'productId' => $productId,
                'quantity'  => 1
            ]);
        }
        else if (array_key_exists($productId, $cart))
        {
            $quantity = $cart[$productId]['quantity'];
            $quantity++;
            $session->write("Cart." . $productId, [
                'productId' => $productId,
                'quantity'  => $quantity
            ]);
        }
        else{
            $session->write("Cart." . $productId, [
                'productId' => $productId,
                'quantity'  => 1
            ]);
        }

        return $this->getCount();
    }
     

    public function getCount() {
        $cart = $this->readCart();
        
        if($cart == null){
            return 0;
        }
        
        $count = 0;
        foreach ($cart as $product) {
            $count += $product['quantity'];
        }
         
        return $count;
    }

    public function checkcart() {
        $cart = $this->readCart();
        
        if($cart == null){
            return false;
        }
        
        foreach ($cart as $product) {
            if($product['quantity'] <= 0)
            {
                return false;
            }
        }
         
        return true;
    }

    public function update($productId, $quantity)
    {
        $session = $this->request->session();
        
        $this->remove($productId);
        for($i = 0; $i < $quantity; $i++)
        {
            $this->add($productId);
        }
    }

    public function remove($productId)
    {
        $session = $this->request->session();

        if ($session->check('Cart.' . $productId)) {
            $session->delete('Cart.' . $productId);
        }
    }

    public function checkifincart($productId = null)
    {
        $session = $this->request->session();

        if($productId == null)
        {
            return false;
        }

        if ($session->check('Cart.' . $productId))
        {
            return true;
        }

        return false;
    }
 
    // Same as readCart but also returns item price totals and quantities
    public function readCartVerbose()
    {
        $cart = $this->readCart();
        $ItemsTable = TableRegistry::get('Items');

       
        $items = array();
        if($cart)
        {
            foreach($cart as $cartItem)
            {
                $cartRow = array();
                $itemprice = 0;
                $item = $ItemsTable->get($cartItem['productId'], ['contain' =>['Photos']]);

                $itemtotal = $item->actualprice * $cartItem['quantity'];
                $discount = ($item->standardprice - $item->actualprice) * $cartItem['quantity'];

                $item->actualprice = number_format((float)($item->actualprice), 2, '.', '');
                $item->standardprice = number_format((float)($item->standardprice), 2, '.', '');
                $item->saleprice = number_format((float)($item->saleprice), 2, '.', '');
                

                $itemtotal = number_format((float)($itemtotal), 2, '.', '');
                $discount = number_format((float)($discount), 2, '.', '');
                
                $cartRow['item'] = $item;
                $cartRow['total'] = $itemtotal;
                $cartRow['discount'] = $discount;
                $cartRow['quantity'] = $cartItem['quantity'];
                array_push($items, $cartRow);
            }
        }

        return $items;
    }

    /*
     * read cart data from session
     */
    public function readCart() {
        $session = $this->request->session();
        if($session){
        return $session->read('Cart');
        }
    }

    public function destroyCart()
    {
        $session = $this->request->session();
        $cart = $session->read('Cart');
        if($cart != null){
            $session->destroy();        
        }
    }


    //////////////////////////////////////////// START SHIPPING /////////////////////////////////////////////////////////////

    public function initializeshipping()
    {
        $session = $this->request->session();
        $shipping = $session->read('Shipping');

        if ($shipping == null){
            $session->write("Shipping.firstname", '');
            $session->write("Shipping.lastname", '');
            $session->write("Shipping.streetaddress", '');
            $session->write("Shipping.city", '');
            $session->write("Shipping.zip", '');
            $session->write("Shipping.state", '');
            $session->write("Shipping.email", '');
            $session->write("Shipping.phone", '');
            $session->write("Shipping.company", '');
            $session->write("Shipping.selection", 0);
        }
    }

    public function checkshipping()
    {
        $session = $this->request->session();
        $shipping = $session->read('Shipping');

        if ($shipping == null)
        {
            $this->initializeshipping();
        }
    }

    public function getshippingsession()
    {
        $session = $this->request->session();
        $shipping = $session->read('Shipping');
        
        return $shipping;
    }


    ////////////////////////////////// End Class Utilities ////////////////////////////////////
    
    ////////////////////////////////// Shipping Selecion and Cost Utilities ////////////////////////////////////
    

    public function getshippingselection()
    {
        $session = $this->request->session();
        $shipping = $session->read('Shipping');

        if ($shipping == null){
            $this->checkshipping();
            return 'standard';
        }
        else if($shipping['selection'] == 0)
        {
            return 'standard';
        }
        else if($shipping['selection'] == 1)
        {
            return 'express';
        }
        else
        {
            return 'twoday';
        }
    }

    public function getshippingcost()
    {
        if($this->getCount() <= 0)
        {
            return '0.00';
        }

        $ConfigsTable = TableRegistry::get('Configs');
         
        $config = $ConfigsTable->find('all')->where(['Id' => '1']);
        $standard = 0;
        $express = 0;
        $twoday = 0;    
        
        foreach($config as $c)
        {
            $standard = $c->standardshipping;
            $express = $c->expressshipping;
            $twoday = $c->twodayshipping;   
        }

        $standard = number_format((float) $standard, 2, '.', '');
        $express = number_format((float) $express, 2, '.', '');
        $twoday = number_format((float) $twoday, 2, '.', '');

        $selection = $this->getshippingselection();
        if($selection === 'standard')
        {
            return $standard;
        }
        elseif($selection === 'express')
        {
            return $express;
        }
        else
        {
            return $twoday;
        }
    }
    
    public function setshippingstandard()
    {
        $session = $this->request->session();
        $session->write("Shipping.selection", 0);
    }
    
    public function setshippingexpress()
    {
        $session = $this->request->session();
        $session->write("Shipping.selection", 1);
    }
    
    public function setshippingtwoday()
    {
        $session = $this->request->session();
        $session->write("Shipping.selection", 2);
    }

}