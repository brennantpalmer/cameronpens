<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class ShippingComponent extends Component
{

    ////////////////////////////////// Class Utilities ////////////////////////////////////
    

    public function initializeshipping()
    {
        $session = $this->request->session();
        $shipping = $session->read('Shipping');

        if ($shipping == null){
            $session->write("Shipping.infoadded", '0');
            $session->write("Shipping.firstname", '');
            $session->write("Shipping.lastname", '');
            $session->write("Shipping.streetaddress", '');
            $session->write("Shipping.city", '');
            $session->write("Shipping.zip", '');
            $session->write("Shipping.state", '');
            $session->write("Shipping.country", '');
            $session->write("Shipping.email", '');
            $session->write("Shipping.phone", '');
            $session->write("Shipping.company", '');
            $session->write("Shipping.selection", 0);
        }
    }

    public function checkshipping()
    {
        $session = $this->request->session();
        $shipping = $session->read('Shipping');

        if ($shipping == null)
        {
            $this->initializeshipping();
            return false;
        }

        return true;
    }

    public function checkinfoadded()
    {
        if($this->checkshipping())
        {
            $session = $this->getshippingsession();
            $infoaddded = $session['infoadded'];
            if($infoaddded === '1')
            {
                return true;
            }
        }

        return false;
    }

    public function getshippingsession()
    {
        $this->checkshipping();

        $session = $this->request->session();
        $shipping = $session->read('Shipping');
        
        return $shipping;
    }

    ////////////////////////////////// End Class Utilities ////////////////////////////////////
    

    ////////////////////////////////// Shipping Address Getters ////////////////////////////////////

    public function getshippingfirstname()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['firstname'];
    }

    public function getshippinglastname()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['lastname'];
    }

    public function getshippingstreetaddress()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['streetaddress'];
    }

    public function getshippingcity()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['city'];
    }

    public function getshippingstate()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['state'];
    }

    public function getshippingcountry()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['country'];
    }

    public function getshippingzip()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['zip'];
    }

    public function getshippingemail()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['email'];
    }

    public function getshippingphone()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['phone'];
    }

    public function getshippingcompany()
    {
        $shippingsession = $this->getshippingsession();
        return $shippingsession['company'];
    }

    ////////////////////////////////// End Shipping Address Getters ////////////////////////////////////    

    ////////////////////////////////// Shipping Address Setters ////////////////////////////////////

    public function setall($data)
    {
        $this->setshippingfirstname($data['firstname']);
        $this->setshippinglastname($data['lastname']);
        $this->setshippingcompany($data['company']);
        $this->setshippingstreetaddress($data['street']);
        $this->setshippingcity($data['city']);
        $this->setshippingzip($data['zip']);
        $this->setshippingstate($data['state']);
        $this->setshippingcountry($data['country']);
        $this->setshippingphone($data['phone']);
        $this->setshippingemail($data['email']);
    }

    public function setshippingfirstname($value)
    {
       
            $session = $this->request->session();
            $session->write("Shipping.firstname", $value);
            $session->write("Shipping.infoadded", '1');
        
    }
    
    public function setshippinglastname($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.lastname", $value);
            $session->write("Shipping.infoadded", '1');     
        
    }

    public function setshippingstreetaddress($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.streetaddress", $value);
            $session->write("Shipping.infoadded", '1');        
        
    }

    public function setshippingcity($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.city", $value);
            $session->write("Shipping.infoadded", '1');
        
    }

    public function setshippingstate($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.state", $value);
            $session->write("Shipping.infoadded", '1');
        
    }

    public function setshippingcountry($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.country", $value);
            $session->write("Shipping.infoadded", '1');
        
    }

    public function setshippingzip($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.zip", $value);
            $session->write("Shipping.infoadded", '1');
        
    }

    public function setshippingcompany($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.company", $value);
            $session->write("Shipping.infoadded", '1');
        
    }

    public function setshippingphone($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.phone", $value);
            $session->write("Shipping.infoadded", '1');
        
    }

    public function setshippingemail($value)
    {
        
            $session = $this->request->session();
            $session->write("Shipping.email", $value);
            $session->write("Shipping.infoadded", '1');
        
    }

    ////////////////////////////////// End Shipping Address Setters ////////////////////////////////////

    ////////////////////////////////// Shipping Validation Helpers ///////////////////////////////////////

    public function validatedata($data)
    {
        $errors = array();

        if(!$this->validateinputstring($data['firstname']))
        {
            array_push($errors, 'First Name is not Valid');
        }

        if(!$this->validateinputstring($data['lastname']))
        {
            array_push($errors, 'Last Name is not Valid');
        }

        if(!$this->validateinputstring($data['street']))
        {
            array_push($errors, 'Street Address is not Valid');
        }

        if(!$this->validateinputstring($data['city']))
        {
            array_push($errors, 'City is not Valid');
        }

        if(!$this->validateinputstring($data['zip']))
        {
            array_push($errors, 'Zip/Postal Code is not Valid');
        }

        if(!$this->validateemail($data['email']))
        {
            array_push($errors, 'Email is not Valid');
        }
       
        if(!$this->validatestate($data['state']))
        {
            array_push($errors, 'State is not Valid');
        }

        if(!$this->validatecountry($data['country']))
        {
            array_push($errors, 'Country is not Valid');
        }

        return $errors;
    }

    public function validateall()
    {
        $errors = array();

        if(!$this->validateinputstring($this->getshippingfirstname()))
        {
            array_push($errors, 'First Name is not Valid');
        }

        if(!$this->validateinputstring($this->getshippinglastname()))
        {
            array_push($errors, 'Last Name is not Valid');
        }

        if(!$this->validateinputstring($this->getshippingstreetaddress()))
        {
            array_push($errors, 'Street Address is not Valid');
        }

        if(!$this->validateinputstring($this->getshippingcity()))
        {
            array_push($errors, 'City is not Valid');
        }

        if(!$this->validateinputstring($this->getshippingzip()))
        {
            array_push($errors, 'Zip/Postal Code is not Valid');
        }

        if(!$this->validateinputstring($this->getshippingemail()) || !filter_var($this->getshippingemail(), FILTER_VALIDATE_EMAIL))
        {
            array_push($errors, 'Email is not Valid');
        }
       
        if(!$this->validatestate($this->getshippingstate()))
        {
            array_push($errors, 'State is not Valid');
        }

        if(!$this->validatecountry($this->getshippingcountry()))
        {
            array_push($errors, 'Country is not Valid');
        }

        return $errors;
    }

    public function validateemail($value)
    {
        if($this->validateinputstring($value) && filter_var($value, FILTER_VALIDATE_EMAIL))
        {
            return true;
        }

        return false;
    }

    public function validateinputstring($value)
    {
        if (ctype_space($value) || $value == '') {
            return false;
        }

        return true;
    }

    public function validatecountry($value)
    {
        $states = array(
            array('CA', 'Canada'),
            array('US', 'United States')
        );

        foreach($states as $state)
        {
            if($value == $state[0] || $value == $state[1])
            {
                return true;
            }
        }

        return false;
    }

    public function validatestate($value)
    {
        $states = array(
            array('AK', 'Alaska'),
            array('AL', 'Alabama'),
            array('AR', 'Arkansas'),
            array('AZ', 'Arizona'),
            array('CA', 'California'),
            array('CO', 'Colorado'),
            array('CT', 'Connecticut'),
            array('DC', 'District of Columbia'),
            array('DE', 'Delaware'),
            array('FL', 'Florida'),
            array('GA', 'Georgia'),
            array('HI', 'Hawaii'),
            array('IA', 'Iowa'),
            array('ID', 'Idaho'),
            array('IL', 'Illinois'),
            array('IN', 'Indiana'),
            array('KS', 'Kansas'),
            array('KY', 'Kentucky'),
            array('LA', 'Louisiana'),
            array('MA', 'Massachusetts'),
            array('MD', 'Maryland'),
            array('ME', 'Maine'),
            array('MI', 'Michigan'),
            array('MN', 'Minnesota'),
            array('MO', 'Missouri'),
            array('MS', 'Mississippi'),
            array('MT', 'Montana'),
            array('NC', 'North Carolina'),
            array('ND', 'North Dakota'),
            array('NE', 'Nebraska'),
            array('NH', 'New Hampshire'),
            array('NJ', 'New Jersey'),
            array('NM', 'New Mexico'),
            array('NV', 'Nevada'),
            array('NY', 'New York'),
            array('OH', 'Ohio'),
            array('OK', 'Oklahoma'),
            array('OR', 'Oregon'),
            array('PA', 'Pennsylvania'),
            array('PR', 'Puerto Rico'),
            array('RI', 'Rhode Island'),
            array('SC', 'South Carolina'),
            array('SD', 'South Dakota'),
            array('TN', 'Tennessee'),
            array('TX', 'Texas'),
            array('UT', 'Utah'),
            array('VA', 'Virginia'),
            array('VT', 'Vermont'),
            array('WA', 'Washington'),
            array('WI', 'Wisconsin'),
            array('ON', 'Ontario'),
            array('QC', 'Quebec'),
            array('NS', 'Nova Scotia'),
            array('NB', 'New Brunswick'),
            array('MB', 'Manitoba'),
            array('BC', 'British Columbia'),
            array('PE', 'Prince Edward Island'),
            array('UT', 'Saskatchewan'),
            array('AB', 'Alberta'),
            array('NL', 'Newfoundland and Labrador')
        );

        foreach($states as $state)
        {
            if($value == $state[0] || $value == $state[1])
            {
                return true;
            }
        }

        return false;
    }

    ////////////////////////////////// END Validation Helpers ///////////////////////////////////////////

    ////////////////////////////////// Shipping Selecion and Cost Utilities ////////////////////////////////////
    

    public function getshippingselection()
    {
        $session = $this->request->session();
        $shipping = $session->read('Shipping');

        if ($shipping == null){
            $this->checkshipping();
            return 'standard';
        }
        else if($shipping['selection'] == 0)
        {
            return 'standard';
        }
        else if($shipping['selection'] == 1)
        {
            return 'express';
        }
        else
        {
            return 'twoday';
        }
    }

    public function getshippingcost()
    {
        $ConfigsTable = TableRegistry::get('Configs');
         
        $config = $ConfigsTable->find('all')->where(['Id' => '1']);
        $standard = 0;
        $express = 0;
        $twoday = 0;    
        
        foreach($config as $c)
        {
            $standard = $c->standardshipping;
            $express = $c->expressshipping;
            $twoday = $c->twodayshipping;   
        }

        $selection = $this->getshippingselection();
        if($selection === 'standard')
        {
            return $standard;
        }
        elseif($selection === 'express')
        {
            return $express;
        }
        else
        {
            return $twoday;
        }
    }
    
    public function setshippingstandard()
    {
        $session = $this->request->session();
        $session->write("Shipping.selection", 0);
    }
    
    public function setshippingexpress()
    {
        $session = $this->request->session();
        $session->write("Shipping.selection", 1);
    }
    
    public function setshippingtwoday()
    {
        $session = $this->request->session();
        $session->write("Shipping.selection", 2);
    }

    ////////////////////////////////// End Shipping Selecion and Cost Utilities ////////////////////////////////////
    

}