<?php
namespace App\Controller;

use App\Controller\AppController;


class ShippingController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow([
            'entershipping',
            'setshippingstandard',
            'setshippingtwoday',
            'setshippingexpress'
            ]);
    }

    public function entershipping()
    {
        if($this->Cart->getCount() <= 0)
        {
            return $this->redirect(['controller'=>'items','action' => 'cart']);
        }

        $cartTotals = $this->Cart->getcarttotals();
        $this->set('cartTotals', $cartTotals);

        $shippingselection = $this->Cart->getshippingselection();
        $this->set('shippingselection', $shippingselection);

        $errors = array();

        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $data = $this->request->data;
            
            if($data['continue'] == 0)
            {
                $this->Shipping->setall($data);
                return $this->redirect(['controller'=>'items','action' => 'cart']);
            }
            
            $this->Shipping->setall($data);
            $errors = $this->Shipping->validateall();
            
            if(!count($errors) > 0)
            {
                return $this->redirect(['controller'=>'orders','action' => 'add']);
            }

            $this->set('errors', $errors);
            
        }
        
        
        $shippinginfo = $this->Shipping->getshippingsession();
        $this->set('shippinginfo', $shippinginfo);
        
        
        
    }


    /////////////////////////////////////////////////////////////////////////////
    //////////////////////*** BEGIN SHIPPING FUNCTIONS   ***/////////////////////
    /////////////////////////////////////////////////////////////////////////////

    public function setshippingstandard()
    {
        $this->autoRender = false;
        $this->Shipping->setshippingstandard();
        return $this->redirect(['action' => 'entershipping']);
    }
    
    public function setshippingexpress()
    {
        $this->autoRender = false;
        $this->Shipping->setshippingexpress();
        return $this->redirect(['action' => 'entershipping']);
        
    }
    
    public function setshippingtwoday()
    {
        $this->autoRender = false;
        $this->Shipping->setshippingtwoday();
        return $this->redirect(['action' => 'entershipping']);
        
    }

    

    ////////////////////////////////////////////////////////////////////////////
    //////////////////////*** END SHIPPING FUNCTIONS   ***//////////////////////
    ////////////////////////////////////////////////////////////////////////////
}
