<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{

    

    public function initialize()
    {
        parent::initialize();

        $this->viewBuilder()->setLayout('admin_layout');        
        $this->loadComponent('Paypal');
        $this->Auth->allow([
            'add',
            'transactionapproved',
            'transactioncancelled'
            ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $orders = $this->paginate($this->Orders);

        $this->set(compact('orders'));
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Orderitems']
        ]);

        $this->set('order', $order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->Cart->getCount() <= 0)
        {
            return $this->redirect(['controller'=>'items','action' => 'cart']);
        }

        $cartTotals = $this->Cart->getcarttotals();
        $this->set('cartTotals', $cartTotals);

        $shippingselection = $this->Cart->getshippingselection();
        $this->set('shippingselection', $shippingselection);

        $Cart = $this->Cart->readCartVerbose();
        $this->set('Cart', $Cart);

        $shippinginfo = $this->Shipping->getshippingsession();
        $this->set('shippinginfo', $shippinginfo);
        
        if ($this->request->is('post')) {

            $data = $this->request->data;
            if(!isset($data['continue']))
            {
                return $this->redirect(['controller'=>'items','action' => 'cart']);
            }

            if($data['continue'] == 0)
            {
                return $this->redirect(['controller'=>'shipping','action' => 'entershipping']);
            }

            $errors = $this->Shipping->validateall();
            if(count($errors) > 0)
            {
                return $this->redirect(['controller'=>'shipping','action' => 'entershipping']);
            }

            if(!$this->Cart->checkcart())
            {
                return $this->redirect(['controller'=>'items','action' => 'index']);
            }

            ///////////////////// Create Order and associated Orderitems ///////////////////////////////
            $order = $this->Orders->newEntity();
            
            $cart = $this->Cart->readcartverbose();

            $order->cptransactionid = uniqid();
            $order->description = "Payment For Items Bought from Cameron Pens (www.cameronpens.com)";
            $order->firstname = $shippinginfo['firstname'];
            $order->lastname = $shippinginfo['lastname'];
            $order->streetaddress = $shippinginfo['streetaddress'];
            $order->company = $shippinginfo['company'];
            $order->city = $shippinginfo['city'];
            $order->state = $shippinginfo['state'];
            $order->country = $shippinginfo['country'];            
            $order->zip = $shippinginfo['zip'];
            $order->email = $shippinginfo['email'];
            $order->phone = $shippinginfo['phone'];
            $order->shipping = $cartTotals['shipping'];
            $order->tax = $cartTotals['tax'];
            $order->subtotal = $cartTotals['subtotal'];
            $order->total = $cartTotals['total'];
            $order->completed = false;
            $order->cancelled = false;
            $order->shipped = false;

            $cartitems = array();
            if ($this->Orders->save($order)) 
            {

                $OrderItemsTable = TableRegistry::get('orderitems');  

                foreach($Cart as $item)
                {
                    $cartitem = $OrderItemsTable->newEntity();
                    $tempArray = array();

                    $tempArray['itemid'] = $item['item']['id'];
                    $tempArray['title'] = $item['item']['title'];
                    $tempArray['price'] = $item['item']['actualprice'];
                    $tempArray['currency'] = 'USD';
                    $tempArray['quantity'] = $item['quantity'];
                    $tempArray['orders']['_ids'][0] = $order->id;

                    $OrderItemsTable->patchEntity($cartitem, $tempArray);
                    if(!$OrderItemsTable->save($cartitem))
                    {
                        $this->Flash->error(__('The transaction could not be completed.'));
                        return $this->redirect(['controller'=>'items','action' => 'index']);
                    }

                    array_push($cartitems, $cartitem);
                }

                
                $this->Paypal->createpayment($cartitems, $cartTotals, $order);
                $paymentArray = $this->Paypal->initializepayment();
              
                $order->paypalpaymentid = $paymentArray['paymentid'];
                if ($this->Orders->save($order))
                {
                    return $this->redirect($paymentArray['url']);                    
                }
            }
            else
            {
                $this->Flash->error(__('The item could not be saved. Please, try again.'));            
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Orderitems']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $orderitems = $this->Orders->Orderitems->find('list', ['limit' => 200]);
        $this->set(compact('order', 'orderitems'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function testpaypal()
    {
        $this->autoRender = false;
        $this->Paypal->intitializeAPI();
        $this->Paypal->createpayment();
        $url = $this->Paypal->initializepayment();
        
        return $this->redirect($url);
    }   

    public function transactioncancelled()
    {
        debug($this->request->getData());
        debug($this->request->getQueryParams());

        $this->Cart->destroyCart();

        $queryParams = $this->request->getQueryParams();

        if(!isset($queryParams['cptransactionid'])){
            debug('Transaction cannot be cancelled because there was no info given: Orders::tranactioncancelled');
            return $this->redirect(['controller' => 'items', 'action' => 'index']);
        }

        $orders = $this->Orders->find('all')->where(['cptransactionid' => $queryParams['cptransactionid']]);
        $order = $orders->first();

        if(!$order){
            debug('No Order Found in Database matching the one supplied: Orders::tranactioncancelled');
            return $this->redirect(['controller' => 'items', 'action' => 'index']);
        }

        if($order->approved){
            debug('This transaction has already been approved: Orders::tranactioncancelled');
            return $this->redirect(['controller' => 'items', 'action' => 'index']);
        }

        if($order->cancelled){
            debug('This transaction has already been cancelled: Orders::tranactioncancelled');
            return $this->redirect(['controller' => 'items', 'action' => 'index']);
        }

        $order->cancelled = true;
        $success = $this->Orders->save($order);

        if(!$success){
            debug('This order could not update its DB entry to "cancelled": Orders::tranactioncancelled');
            return $this->redirect(['controller' => 'items', 'action' => 'index']);
        }
    }

    public function transactionapproved()
    {
        
        if(!$this->Paypal->checkBeforeExec())
        {
            return $this->redirect(['controller' => 'items', 'action' => 'index']);
        }

        $result = $this->Paypal->execute();
        $this->Cart->destroyCart();

        
        $query = $this->Orders->find('all')->where(['paypalpaymentid' => $result['result']->id])->contain('orderitems');        
        $order = $query->first();

        if(!$order){
            debug('No Order Found in Database matching the one supplied: Orders::tranactionapproved');
            return $this->redirect(['controller' => 'items', 'action' => 'index']);
        }

       if($result['result']->state === 'approved')
       {
           $order->paypaltransactionid = $result['transactionid'];
           $order->paypalpayerid = $result['payerid'];     
           $order->approved = true;      
           if(!$this->Orders->save($order))
           {
            throw new \Cake\Core\Exception\Exception(__('Unable to save order'));
           }
       }
       
       $this->set('order', $order);
    }
}
