<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ItemsPhotos Controller
 *
 * @property \App\Model\Table\ItemsPhotosTable $ItemsPhotos
 *
 * @method \App\Model\Entity\ItemsPhoto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemsPhotosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Items', 'Photos']
        ];
        $itemsPhotos = $this->paginate($this->ItemsPhotos);

        $this->set(compact('itemsPhotos'));
    }

    /**
     * View method
     *
     * @param string|null $id Items Photo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemsPhoto = $this->ItemsPhotos->get($id, [
            'contain' => ['Items', 'Photos']
        ]);

        $this->set('itemsPhoto', $itemsPhoto);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $itemsPhoto = $this->ItemsPhotos->newEntity();
        if ($this->request->is('post')) {
            $itemsPhoto = $this->ItemsPhotos->patchEntity($itemsPhoto, $this->request->getData());
            if ($this->ItemsPhotos->save($itemsPhoto)) {
                $this->Flash->success(__('The items photo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The items photo could not be saved. Please, try again.'));
        }
        $items = $this->ItemsPhotos->Items->find('list', ['limit' => 200]);
        $photos = $this->ItemsPhotos->Photos->find('list', ['limit' => 200]);
        $this->set(compact('itemsPhoto', 'items', 'photos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Items Photo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemsPhoto = $this->ItemsPhotos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemsPhoto = $this->ItemsPhotos->patchEntity($itemsPhoto, $this->request->getData());
            if ($this->ItemsPhotos->save($itemsPhoto)) {
                $this->Flash->success(__('The items photo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The items photo could not be saved. Please, try again.'));
        }
        $items = $this->ItemsPhotos->Items->find('list', ['limit' => 200]);
        $photos = $this->ItemsPhotos->Photos->find('list', ['limit' => 200]);
        $this->set(compact('itemsPhoto', 'items', 'photos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Items Photo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $itemsPhoto = $this->ItemsPhotos->get($id);
        if ($this->ItemsPhotos->delete($itemsPhoto)) {
            $this->Flash->success(__('The items photo has been deleted.'));
        } else {
            $this->Flash->error(__('The items photo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
