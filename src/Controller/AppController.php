<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Cookie');
        $this->loadComponent('Flash');
        $this->loadComponent('Cart');
        $this->loadComponent('Shipping');
        
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ],
                    'userModel' => 'Admins'
                ]
            ],
            'loginAction' => [
                'controller' => 'admins',
                'action' => 'login'
            ],
             // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => $this->referer()
        ]);
    }

    public function beforeFilter(Event $event)
	{
        parent::beforeFilter($event);

        // Set the cartCount variable for use when displaying the number of items in the cart over the scope of the application
        $cartCount = $this->Cart->getCount();
        $this->set('cartCount', $cartCount);

        $shippingSelection = $this->Shipping->getshippingselection();
        $this->set('shippingSelection', $shippingSelection);

        $categoriesTable = TableRegistry::get('Categories');
        $global_categories = $categoriesTable->find('all');
        $this->set(compact('global_categories'));
        
        $materialsTable = TableRegistry::get('Materials');
        $global_materials = $materialsTable->find('all');
        $this->set(compact('global_materials'));

        $adminloggedin = false;
        if ($this->Auth->user()) 
        {
            $adminloggedin = true;
        }
        $this->set('adminloggedin', $adminloggedin);

        $controllerName = $this->request->getParam('controller');
        $actionName = $this->request->getParam('action');
        

        $ConfigsTable = TableRegistry::get('Configs');
        $configs = $ConfigsTable->find('all')->first();

        
        

            if($controllerName == 'Admins' && $configs->maintenancemode === true)
            {
                return;
            }

            if($controllerName == 'Configs' && $configs->maintenancemode === true)
            {
                return;
            }

            if(!$adminloggedin)
            {
                if($configs->maintenancemode === true)
                {
                    
                        //redirect to maintenance page
                        return $this->redirect(['controller' => 'configs', 'action' => 'siteundermaintenance']);
                }
            }
        
        
    }   

}
