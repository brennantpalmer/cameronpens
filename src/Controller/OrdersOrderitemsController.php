<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdersOrderitems Controller
 *
 * @property \App\Model\Table\OrdersOrderitemsTable $OrdersOrderitems
 *
 * @method \App\Model\Entity\OrdersOrderitem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersOrderitemsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Orders', 'Orderitems']
        ];
        $ordersOrderitems = $this->paginate($this->OrdersOrderitems);

        $this->set(compact('ordersOrderitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Orders Orderitem id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordersOrderitem = $this->OrdersOrderitems->get($id, [
            'contain' => ['Orders', 'Orderitems']
        ]);

        $this->set('ordersOrderitem', $ordersOrderitem);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordersOrderitem = $this->OrdersOrderitems->newEntity();
        if ($this->request->is('post')) {
            $ordersOrderitem = $this->OrdersOrderitems->patchEntity($ordersOrderitem, $this->request->getData());
            if ($this->OrdersOrderitems->save($ordersOrderitem)) {
                $this->Flash->success(__('The orders orderitem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The orders orderitem could not be saved. Please, try again.'));
        }
        $orders = $this->OrdersOrderitems->Orders->find('list', ['limit' => 200]);
        $orderitems = $this->OrdersOrderitems->Orderitems->find('list', ['limit' => 200]);
        $this->set(compact('ordersOrderitem', 'orders', 'orderitems'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Orders Orderitem id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordersOrderitem = $this->OrdersOrderitems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordersOrderitem = $this->OrdersOrderitems->patchEntity($ordersOrderitem, $this->request->getData());
            if ($this->OrdersOrderitems->save($ordersOrderitem)) {
                $this->Flash->success(__('The orders orderitem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The orders orderitem could not be saved. Please, try again.'));
        }
        $orders = $this->OrdersOrderitems->Orders->find('list', ['limit' => 200]);
        $orderitems = $this->OrdersOrderitems->Orderitems->find('list', ['limit' => 200]);
        $this->set(compact('ordersOrderitem', 'orders', 'orderitems'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Orders Orderitem id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordersOrderitem = $this->OrdersOrderitems->get($id);
        if ($this->OrdersOrderitems->delete($ordersOrderitem)) {
            $this->Flash->success(__('The orders orderitem has been deleted.'));
        } else {
            $this->Flash->error(__('The orders orderitem could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
