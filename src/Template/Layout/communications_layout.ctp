<?php


$cameronpensDescription = 'Cameron Pens: Quality hand crafted pens at great prices!';
?>
<!DOCTYPE html>
<html>
<head>
    
    <?= $this->element('head', ['cameronpensDescription' => $cameronpensDescription]) ?>

</head>
<body>
    
    <!-- *** TOPBAR *** -->
    <?= $this->element('topbar') ?>
    <!-- *** TOPBAR END *** -->

    <!-- *** NAVBAR *** -->
    <?= $this->element('navbar') ?>
    <!-- *** NAVBAR END *** -->

    <div id="all">
        <!-- CONTENT -->
        <div id="content">

            <div class="container">

                <!-- *** MENUS AND FILTERS *** -->
                <?= $this->element('communications_layout/communications_sidebar') ?>
                <!-- *** MENUS AND FILTERS END *** -->

                <div class="col-md-9">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?> 
                </div>
                <!-- col-md-9 END-->     
            </div>
            <!-- CONTAINER END-->            
        </div>
        <!-- CONTENT END-->

        <!-- *** FOOTER *** -->
        <?= $this->element('footer') ?>
        <!-- *** FOOTER END *** -->

        <!-- *** COPYRIGHT *** -->
        <?= $this->element('copywrite') ?>
        <!-- *** COPYRIGHT END *** -->


    </div>
    <!-- /#all -->
    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <?= $this->Html->script('jquery-1.11.0.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('jquery.cookie.js') ?>
    <?= $this->Html->script('waypoints.min.js') ?>
    <?= $this->Html->script('modernizr.js') ?>
    <?= $this->Html->script('bootstrap-hover-dropdown.js') ?>
    <?= $this->Html->script('owl.carousel.min.js') ?>
    <?= $this->Html->script('front.js') ?>


</body>

</html>

