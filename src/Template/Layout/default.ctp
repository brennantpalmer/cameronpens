<?php


$cameronpensDescription = 'Cameron Pens: Quality hand crafted pens at great prices!';
?>
<!DOCTYPE html>
<html>
<head>
    
    <?= $this->element('head', ['cameronpensDescription' => $cameronpensDescription]) ?>

</head>
<body>
    
    <!-- *** TOPBAR *** -->
    <?= $this->element('topbar') ?>
    <!-- *** TOPBAR END *** -->

    <!-- *** NAVBAR *** -->
    <?= $this->element('navbar') ?>
    <!-- *** NAVBAR END *** -->

    <div id="all">
        <!-- CONTENT -->
        <div id="content">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>          
        </div>
        <!-- CONTENT END-->


        <!-- *** FOOTER *** -->
        <?= $this->element('footer') ?>
        <!-- *** FOOTER END *** -->

        <!-- *** COPYRIGHT *** -->
        <?= $this->element('copywrite') ?>
        <!-- *** COPYRIGHT END *** -->

    </div>
    <!-- /#all -->
    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <?= $this->Html->script('jquery-1.11.0.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('jquery.cookie.js') ?>
    <?= $this->Html->script('waypoints.min.js') ?>
    <?= $this->Html->script('modernizr.js') ?>
    <?= $this->Html->script('bootstrap-hover-dropdown.js') ?>
    <?= $this->Html->script('owl.carousel.min.js') ?>
    <?= $this->Html->script('front.js') ?>
    <script>
$(document).ready(function()
{
    function convertToSlug()
    {
        var title = $("#title").val();
        $("#slug").val(title.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,''));
    }
    $(document).on("change, keyup", "#title", convertToSlug);
});
</script>

<script>

$(document).ready(function(){
    $('#add-form').submit(function(e){
        e.preventDefault();
        var tis = $(this);
        $.post(tis.attr('action'),tis.serialize(),function(data){
            $('#cart-counter').text(data);
        });
    });
});
</script>

</body>

</html>

