<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>

<div class="container">
    <div class="items form large-9 medium-8 columns content">
        <?= $this->Form->create($item, ['type' => 'file']) ?>
        <fieldset class="form-group">
            <legend><?= __('Add Item') ?></legend>
            <?php

                function frand($min, $max, $decimals = 0) {
                    $scale = pow(10, $decimals);
                    return mt_rand($min * $scale, $max * $scale) / $scale;
                }
                
                $randTitle = '';
                $randSlug = '';
                $randDescription = '';
                $randOnSale = '';
                $randActive = '';
                $randFeatured = '';
                $randStandardPrice = '';
                $randSalePrice = '';
                $randProductionTime = '';
                $randNumberInStock = '';
            
                if(false){
                $randTitle = 'Pen Title_' . rand(0,9999);
                $randSlug = $randTitle . '_slug';
                $randDescription = 'Pen Description ' . rand(0,9999) . ' Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
                $randOnSale = rand(0,1);
                $randActive = 1;
                $randFeatured = rand(0,1);
                $randStandardPrice = frand(50, 500, 2);
                $randSalePrice = frand(0, $randStandardPrice, 2);
                $randProductionTime = rand(1,10);
                $randNumberInStock = rand(1,10);
                }       
                ?>

                <span>Title</span> <?= $this->Form->control('title', ['id' => 'title', 'default' => $randTitle, 'label' => false]) ?>
                <hr>
                <span>Slug</span> <?= $this->Form->control('slug', ['id' => 'slug', 'readonly' => 'readonly', 'value' => '', 'label' => false]) ?>
                <hr>
                <span>Description</span> <?= $this->Form->control('description', ['default' => $randDescription, 'label' => false]) ?>
                <hr>
                <span>On Sale?</span> <?= $this->Form->control('onsale', ['default' => $randOnSale, 'label' => false]) ?>
                <br>
                <hr>
                <span>Active?</span> <?= $this->Form->control('active', ['default' => $randActive, 'label' => false]) ?>
                <br>
                <hr>
                <span>Featured?</span> <?= $this->Form->control('featured', ['default' => $randFeatured, 'label' => false]) ?>
                <br>
                <hr>
                <span>Is A Bundle?</span> <?= $this->Form->control('bundle', ['default' => $randFeatured, 'label' => false]) ?>
                <br>
                <hr>
                <span>Is A Favorite?</span> <?= $this->Form->control('favorite', ['default' => $randActive, 'label' => false]) ?>
                <br>
                <hr>
                <span>Is A Hot Item?</span> <?= $this->Form->control('hot', ['default' => $randFeatured, 'label' => false]) ?>
                <br>
                <hr>
                <span>Standard(List) Price</span> <?= $this->Form->input('standardprice', ['default' => $randStandardPrice, 'label' => false]) ?>
                <hr>
                <span>Sale Price</span> <?= $this->Form->input('saleprice', ['default' => $randSalePrice, 'label' => false]) ?>
                <hr>
                <span>Time To Produce(For one item)</span> <?= $this->Form->control('productiontime', ['default' => $randProductionTime, 'label' => false]) ?>
                <hr>
                <span>Current Number In Stock</span> <?= $this->Form->control('numberinstock', ['default' => $randNumberInStock, 'label' => false]) ?>
                <hr>
                <span>Materials</span> <?= $this->Form->control('materials._ids', ['options' => $materials, 'label' => false]) ?>
                <hr>
                <span>Pictures</span> <?= $this->Form->control('pictures[]', ['label' => 'Pictures','type' => 'file', 'multiple', 'label' => false]) ?>
                <hr>
                <span>Tags</span> <?= $this->Form->control('tags._ids', ['options' => $tags, 'label' => false]) ?>
                <hr>
                <span>Categories</span> <?= $this->Form->control('categories._ids', ['options' => $categories, 'label' => false]) ?>
                
        </fieldset >
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>


















