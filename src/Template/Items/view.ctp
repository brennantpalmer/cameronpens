<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>

<div class="col-md-9">

    <div class="row" id="productMain">
        <div class="col-sm-6">
            <div id="mainImage">

                <?php $photos = array_values($item->photos); ?>
                <?= $this->Html->image('/files/Photos/title/detailbig-' . $photos[0]['title'], ['alt' => $photos[0]['title'], 'class'=>'img-responsive', 'id' => 'LargePicture']) ?>
            </div>

                        <!-- The Modal -->
            <div id="EnlargePic" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
            </div>

            <?php if($item->onsale) :?>
            <div class="ribbon sale">
                <div class="theribbon">ON SALE!</div>
                <div class="ribbon-background"></div>
            </div>
            <!-- /.ribbon -->
            <?php endif; ?>

        </div>
        
        <div class="col-sm-6">
            <div class="box" id="totals">
                <h2 class="text-center">
                    <?= h($item->title)?>
                </h2>
                  
                <?php if($item->onsale) :?>
                <h4 class="price">
                <del><span style="color: #999999; font-weight: lighter">$<?= h($item->standardprice)?></span></del>
                </h4>
                <?php endif; ?>
                
                <?php if($item->onsale) :?>
                <p class="saleprice">
                    $<?= h($item->actualprice)?>  (<?=$percentageoff?>% OFF!) 
                </p>
                <?php endif; ?>

                <?php if(!$item->onsale) :?>
                <p class="price">
                    $<?= h($item->actualprice)?>
                </p>
                <?php endif; ?>

                <div class = "text-center">
                <p class="text-center buttons">

                    <?php echo $this->Form->create('Item',array('id'=>'add-form','url'=>array('controller'=>'items','action'=>'addToCart', $item->id)));?>
                    <?php echo $this->Form->hidden('productID',array('value'=> $item->id))?>
                    <?php if($isincart)
                    {
                        $proceedocheckoutlink = "Proceed to Checkout?";
                        $cartmessage = 'Item in cart!';
                    }
                    else
                    {
                        $cartmessage = 'Add to cart?';
                        $proceedocheckoutlink = '';
                    } 
                    ?>
                    <?= $this->Form->button(__('<i class="fa fa-shopping-cart"></i>' . ' <span id="cartmessage">' . $cartmessage  . '</span>'), ['class'=>'btn btn-primary'], ['escape' => false]) ?>
                        <?php echo $this->Form->end();?>
                        
                        <?php if($adminloggedin):?>
                        <h4 style="color:blue">
                        <?= $this->Html->link(__('Edit Item'), ['action' => 'edit', $item->id]) ?>   
                        </h4>
                        <h4 style="color:red">
                        <?= $this->Form->postLink(__('Delete Item'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?>
                        </h4>
                <?php endif;?>
                </p>

                <div id="spinner" class="spinner" style="display:none;">
    <img id="img-spinner" src="/img/spinner.gif" alt="Loading"/>
</div>

                
                    
                <p class="text-center buttons">
                    <h4>
                    <span><a href="/items/cart" class="standardlink" id="proceedtocheckoutlink"><?= $proceedocheckoutlink ?></a></span>
                    </h4>
                </p>

                <h4 class="goToDescription">
                    <a href="#details" class="scroll-to">View Product Details</a>
                </h4>
                </div>


            </div>

            <div class="row" id="thumbs">

                <?php foreach($item->photos as $picture) : ?>
                <div class="col-xs-4">
                    <a href="/files/Photos/title/<?= $picture->title ?>" class="thumb">
                        <?=  $this->Html->image('/files/Photos/title/detailsmall-' . $picture->title, ['alt' => $picture->title, 'class'=>'img-responsive']) ?>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>


    <div class="box" id="details">
        <p>
            <h4>Description</h4>
            <p>
                <?= $item->description ?>
            </p>
            <h4>Material(s)</h4>
            <ul>
                <?php foreach($item->materials as $mat) : ?>
                <li>
                    <?= $mat->title ?>
                </li>
                <?php endforeach; ?>
            </ul>
            <h4>Category</h4>
            <ul>
                <?php foreach($item->categories as $cat) : ?>
                <li>
                    <?= $cat->title ?>
                </li>
                <?php endforeach; ?>
            </ul>
            <h4>Tag(s):</h4>
            <ul>
                <p>
                <?php $i = 0;?>
                <?php foreach($item->tags as $tag) : ?>
                    <?php if($i > 0):?>
                        <?= '| ' ?>
                    <?php endif;?>
                    <?= $tag->title . ' ' ?>
                <?php $i++;?>
                <?php endforeach; ?>
                </p>
            </ul>
    </div>

    <div class="row same-height-row">
        <div class="col-md-3 col-sm-6">
            <div class="box same-height" id="item_view_browse">
                <h3>You may also like these products</h3>
            </div>
        </div>

        <?php 
            $counter = 0;
        ?>
        <?php foreach ($itemsyoumaylike as $i): ?>
            <div class="col-md-3 col-sm-6">
                <div class="product same-height">
                    <!-- *** Item *** -->
                    <?= $this->element('item_view_browse', ['item' => $i]) ?>
                        <!-- *** Item END *** -->
                </div>
            </div>

        <?php $counter++; if($counter > 2){ break;} ?>
        <?php endforeach; ?>

    </div>

    <div class="row same-height-row">
        <div class="col-md-3 col-sm-6">
            <div class="box same-height" id="item_view_browse">
                <h3>Products viewed recently by other visitors</h3>
            </div>
        </div>
        
        <?php 
            $counter = 0;
        ?>
        <?php foreach ($itemsrecentlyviewed as $i): ?>
            <div class="col-md-3 col-sm-6">
                <div class="product same-height">
                    <!-- *** Item *** -->
                    <?= $this->element('item_view_browse', ['item' => $i]) ?>
                        <!-- *** Item END *** -->
                </div>
            </div>

        <?php $counter++; if($counter > 2){ break;} ?>
        <?php endforeach; ?>

    </div>

</div>
<!-- /.col-md-9 -->

