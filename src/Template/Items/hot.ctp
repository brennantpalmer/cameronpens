<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item[]|\Cake\Collection\CollectionInterface $items
 */
?>

<div class="box">
            <h1>Hot Products</h1>
        </div>

<div class="box info-bar" id="item_browse_heading">
    <div class="row">
        <div class="col-sm-12 col-md-4 products-showing">
            Showing
            <?= $this->Paginator->counter(['format' => __(' <strong>{{current}}</strong> product(s) out of <strong>{{count}}</strong> total')]) ?>
        </div>

        <div class="col-sm-12 col-md-8  products-number-sort">
            <div class="row">
                <form class="form-inline">
                    <div class="col-md-6 col-sm-6">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="products-sort-by">
                            <strong>Sort by</strong>

                            <!-- Start sort by title -->

                            <?php if ($this->Paginator->sortKey() === 'Items.title'): ?>
                            <strong><?= $this->Paginator->sort('title', 'Name') ?></strong>
                            <i class='fa fa-sort-<?php echo $this->Paginator->sortDir() === 'asc' ? 'up' : 'down'; ?>'></i>
                            <?php else: ?>
                            <?= $this->Paginator->sort('title', 'Name') ?>
                            <i class='fa fa-sort'></i> 
                            <?php endif; ?>
                            <strong> | </strong> 

                            <!-- End sort by title -->

                            <!-- Start sort by price -->

                            <?php if ($this->Paginator->sortKey() === 'Items.actualprice'): ?>
                            <strong><?= $this->Paginator->sort('actualprice', 'Price') ?></strong>
                            <i class='fa fa-sort-<?php echo $this->Paginator->sortDir() === 'asc' ? 'up' : 'down'; ?>'></i>
                            <?php else: ?>
                            <?= $this->Paginator->sort('actualprice', 'Price') ?>
                            <i class='fa fa-sort'></i> 
                            <?php endif; ?>
                            <strong> | </strong> 

                            <!-- End sort by price -->

                            <!-- Start sort by created -->

                            <?php if ($this->Paginator->sortKey() === 'Items.created'): ?>
                            <strong><?= $this->Paginator->sort('created', 'Date') ?></strong>
                            <i class='fa fa-sort-<?php echo $this->Paginator->sortDir() === 'asc' ? 'up' : 'down'; ?>'></i>
                            <?php else: ?>
                            <?= $this->Paginator->sort('created', 'Date') ?>
                            <i class='fa fa-sort'></i> 
                            <?php endif; ?>

                            <!-- End sort by created -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row products">
    <?php foreach ($items as $item): ?>

        <div class="col-md-4 col-sm-6">

                <!-- *** Item *** -->
                    <?= $this->element('item_browse', ['item' => $item]) ?>
                <!-- *** Item END *** -->
           
        </div>

    <?php endforeach; ?>
</div>
<!-- /.products -->

<div class="pages">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    
</div>


