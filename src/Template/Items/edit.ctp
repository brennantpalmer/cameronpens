<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>

<div class="container">
    <div class="items form large-9 medium-8 columns content">
        <?= $this->Form->create($item) ?>
        <fieldset>
            <legend><?= __('Edit Item') ?></legend>
            <?php
                echo $this->Form->control('title');
                echo $this->Form->control('slug');
                echo $this->Form->control('description');
                echo $this->Form->control('onsale');
                echo $this->Form->control('active');
                echo $this->Form->control('featured');
                echo $this->Form->control('favorite');
                echo $this->Form->control('hot');
                echo $this->Form->control('bundle');
                echo $this->Form->control('standardprice');
                echo $this->Form->control('saleprice');
                echo $this->Form->control('productiontime');
                echo $this->Form->control('numberinstock');
                echo $this->Form->control('materials._ids', ['options' => $materials]);
                echo $this->Form->control('photos._ids', ['options' => $photos]);
                echo $this->Form->control('tags._ids', ['options' => $tags]);
                echo $this->Form->control('categories._ids', ['options' => $categories]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>