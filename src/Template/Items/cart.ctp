<div class="container">

    <div class="col-md-9" id="basket">

        <div class="box">

            <?= $this->Form->create(null, ['url' => ['controller' => 'Items', 'action' => 'updateCart']]) ?>

                <h1>Shopping cart</h1>
                <p class="text-muted">You currently have
                    <?= $cartCount ?> item(s) in your cart.</p>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2">Product</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Discount</th>
                                <th colspan="2">Total</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($Cart as $item): ?>

                                <tr>
                                    <td>
                                        <a href="#">
                                            <?= $this->Html->link(($this->Html->image('/files/Photos/title/detailsmall-' . $item['item']['photos'][0]['title'], ['alt' => $item['item']['photos'][0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item['item']['id']],['escape' => false]) ?>
                                        </a>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(__(h($item['item']['title'])), ['action' => 'view', $item['item']['id']]) ?>
                                    </td>
                                    <td>
                                        <input name="<?= $item['item']['id']?>" id="number" type="number" value="<?= $this->Number->format($item['quantity']) ?>">
                                    </td>
                                    <td>
                                        <?= '$' . h($item['item']['standardprice']) ?>
                                    </td>
                                    <td>
                                        <?= '$' . $item['discount'] ?>
                                    </td>
                                    <td>
                                        <?= '$' . $item['total'] ?>
                                    </td>
                                    <td>
                                        <?= $this->Html->link('remove', '/items/removefromcart/' . $item['item']['id']) ?>
                                    </td>
                                </tr>

                                
                                <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="5">Total</th>
                                <th colspan="2">$
                                    <?=$cartTotals['subtotal']?>
                                </th>
                            </tr>
                        </tfoot>
                    </table>

                </div>
                <!-- /.table-responsive -->

                <div class="box-footer">
                    <div class="pull-left">
                        <a href="/items" class="btn btn-default">
                            <i class="fa fa-chevron-left"></i> Continue shopping</a>
                    </div>
                    <div class="pull-right">
                        <?php if($cartCount > 0) : ?>
                            <button class="btn btn-default">
                                <i class="fa fa-refresh"></i> Update basket
                            </button>
                                <a href="/shipping/entershipping" class="btn btn-primary">
                            <i class="fa fa-chevron-right"></i> Proceed to checkout</a>
                            </button>
                        <?php endif; ?>

                        <?= $this->Form->end() ?>

                    </div>
                </div>

        </div>
        <!-- /.box -->


    </div>
    <!-- /.col-md-9 -->

    

    <div class="col-md-3">
        
    <!-- *** Order Summary Side Bar *** -->
    <?= $this->element('checkout_layout/order_summary_sidebar', ['cartTotals' => $cartTotals]) ?>
    <!-- *** END Order Summary Side Bar *** -->

    </div>
    <!-- /.col-md-3 -->

</div>

