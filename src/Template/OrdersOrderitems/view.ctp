<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrdersOrderitem $ordersOrderitem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Orders Orderitem'), ['action' => 'edit', $ordersOrderitem->order_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Orders Orderitem'), ['action' => 'delete', $ordersOrderitem->order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $ordersOrderitem->order_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Orders Orderitems'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Orders Orderitem'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orderitems'), ['controller' => 'Orderitems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Orderitem'), ['controller' => 'Orderitems', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordersOrderitems view large-9 medium-8 columns content">
    <h3><?= h($ordersOrderitem->order_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Order') ?></th>
            <td><?= $ordersOrderitem->has('order') ? $this->Html->link($ordersOrderitem->order->id, ['controller' => 'Orders', 'action' => 'view', $ordersOrderitem->order->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Orderitem') ?></th>
            <td><?= $ordersOrderitem->has('orderitem') ? $this->Html->link($ordersOrderitem->orderitem->title, ['controller' => 'Orderitems', 'action' => 'view', $ordersOrderitem->orderitem->id]) : '' ?></td>
        </tr>
    </table>
</div>
