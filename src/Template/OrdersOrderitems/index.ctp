<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrdersOrderitem[]|\Cake\Collection\CollectionInterface $ordersOrderitems
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Orders Orderitem'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orderitems'), ['controller' => 'Orderitems', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Orderitem'), ['controller' => 'Orderitems', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordersOrderitems index large-9 medium-8 columns content">
    <h3><?= __('Orders Orderitems') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('order_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('orderitem_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersOrderitems as $ordersOrderitem): ?>
            <tr>
                <td><?= $ordersOrderitem->has('order') ? $this->Html->link($ordersOrderitem->order->id, ['controller' => 'Orders', 'action' => 'view', $ordersOrderitem->order->id]) : '' ?></td>
                <td><?= $ordersOrderitem->has('orderitem') ? $this->Html->link($ordersOrderitem->orderitem->title, ['controller' => 'Orderitems', 'action' => 'view', $ordersOrderitem->orderitem->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordersOrderitem->order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordersOrderitem->order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordersOrderitem->order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $ordersOrderitem->order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
