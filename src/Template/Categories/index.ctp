<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\category[]|\Cake\Collection\CollectionInterface $categories
 */
?>

<div class="container">

    <div class="col-md-12">

        <div class="box" id="categories">
            <h1>Browse Categories</h1>
        </div>

        

        <div class="row products">

        <?php foreach ($categories as $category) : ?>
        
            <div class="col-md-3 col-sm-4">
                <div class="product">
                    <div class="flip-container">
                        <div class="flipper">
                            <div class="front">
                            
                            </div>
                            <div class="back">
                            
                            </div>
                        </div>
                    </div>
                    

                    <div class="text">
                        <h3>
                            <?= $this->Html->link(__($category->title), ['action' => 'view', $category->id]) ?>
                        </h3>
                        
                        <?php if($adminloggedin):?>
                        <?= $this->Html->link(__('View'), ['action' => 'view', $category->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $category->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id)]) ?>
                        <?php endif;?>
                        
                    
                        
                        
                    </div>
                    <!-- /.text -->
                </div>
                <!-- /.product -->
            </div>

        <?php endforeach; ?>

        </div>
        <!-- /.products -->
    </div>
</div>
