<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item[]|\Cake\Collection\CollectionInterface $items
 */
?>

<div class="container">

    <div class="col-md-12">

        <div class="box" id="categories">
            <h1>
                <?= $category->title ?>
            </h1>
            <p>
            <?= $this->Text->autoParagraph(h($category->description)); ?>
            </p>
        </div>

        <?php if(empty($category->items)){ echo '<br><p>We\'re sorry, it appears there are no items added in this section. <a class="standardlink" href="/items">Return to store</a></p><br>';} ?>

        <?php foreach ($category->items as $item): ?>
        <div class="col-md-3 col-sm-4">
        <!-- *** Item *** -->
        <?= $this->element('item_browse', ['item' => $item]) ?>
            <!-- *** Item END *** -->
            </div>
        <?php endforeach; ?>


    </div>
</div>



