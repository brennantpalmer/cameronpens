<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsPhoto[]|\Cake\Collection\CollectionInterface $itemsPhotos
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Items Photo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Photos'), ['controller' => 'Photos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Photo'), ['controller' => 'Photos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="itemsPhotos index large-9 medium-8 columns content">
    <h3><?= __('Items Photos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('item_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('photo_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($itemsPhotos as $itemsPhoto): ?>
            <tr>
                <td><?= $itemsPhoto->has('item') ? $this->Html->link($itemsPhoto->item->title, ['controller' => 'Items', 'action' => 'view', $itemsPhoto->item->id]) : '' ?></td>
                <td><?= $itemsPhoto->has('photo') ? $this->Html->link($itemsPhoto->photo->title, ['controller' => 'Photos', 'action' => 'view', $itemsPhoto->photo->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $itemsPhoto->item_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $itemsPhoto->item_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $itemsPhoto->item_id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemsPhoto->item_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
