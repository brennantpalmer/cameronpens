<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsPhoto $itemsPhoto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $itemsPhoto->item_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $itemsPhoto->item_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Items Photos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Photos'), ['controller' => 'Photos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Photo'), ['controller' => 'Photos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="itemsPhotos form large-9 medium-8 columns content">
    <?= $this->Form->create($itemsPhoto) ?>
    <fieldset>
        <legend><?= __('Edit Items Photo') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
