<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsPhoto $itemsPhoto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Items Photo'), ['action' => 'edit', $itemsPhoto->item_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Items Photo'), ['action' => 'delete', $itemsPhoto->item_id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemsPhoto->item_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Items Photos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Items Photo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Photos'), ['controller' => 'Photos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Photo'), ['controller' => 'Photos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="itemsPhotos view large-9 medium-8 columns content">
    <h3><?= h($itemsPhoto->item_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Item') ?></th>
            <td><?= $itemsPhoto->has('item') ? $this->Html->link($itemsPhoto->item->title, ['controller' => 'Items', 'action' => 'view', $itemsPhoto->item->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Photo') ?></th>
            <td><?= $itemsPhoto->has('photo') ? $this->Html->link($itemsPhoto->photo->title, ['controller' => 'Photos', 'action' => 'view', $itemsPhoto->photo->id]) : '' ?></td>
        </tr>
    </table>
</div>
