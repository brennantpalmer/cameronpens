<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material[]|\Cake\Collection\CollectionInterface $materials
 */
?>

<div class="container">

    <div class="col-md-12">

        <div class="box" id="materials">
            <h1>Browse Materials</h1>
            <p>We use only the finest materials when making our products</p>
        </div>

        

        <div class="row products">

        <?php foreach ($materials as $material) : ?>
        
            <div class="col-md-3 col-sm-4">
                <div class="product">
                    <div class="flip-container">
                        <div class="flipper">
                            <div class="front">
                            
                            </div>
                            <div class="back">
                            
                            </div>
                        </div>
                    </div>
                    

                    <div class="text">
                        <h3>
                            <?= $this->Html->link(__($material->title), ['action' => 'view', $material->id]) ?>
                        </h3>

                        <?php if($adminloggedin):?>
                        <?= $this->Html->link(__('View'), ['action' => 'view', $material->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $material->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $material->id], ['confirm' => __('Are you sure you want to delete # {0}?', $material->id)]) ?>
                    
                        <?php endif;?>
                    </div>
                    <!-- /.text -->
                </div>
                <!-- /.product -->
            </div>

        <?php endforeach; ?>

        </div>
        <!-- /.products -->
    </div>
</div>
