<?php

/**
 * States Dropdown 
 *
 * @uses check_select
 * @param string $post, the one to make "selected"
 * @param string $type, by default it shows abbreviations. 'abbrev', 'name' or 'mixed'
 * @return string
 */
function CountryDropdown($post=null, $type='abbrev') {
	$states = array(
		array('CA', 'Canada'),
		array('US', 'United States')
	);
	
	$options = '<option value=""></option>';
	
	foreach ($states as $state) {
		if ($type == 'abbrev') {
    	$options .= '<option value="'.$state[0].'" '. check_select($post, $state[0], false) .' >'.$state[0].'</option>'."\n";
    } elseif($type == 'name') {
    	$options .= '<option value="'.$state[1].'" '. check_select($post, $state[1], false) .' >'.$state[1].'</option>'."\n";
    } elseif($type == 'mixed') {
    	$options .= '<option value="'.$state[0].'" '. check_select($post, $state[0], false) .' >'.$state[1].'</option>'."\n";
    }
	}
		
	echo $options;
}

function StateDropdown($post=null, $type='abbrev') {
	$states = array(
		array('AK', 'Alaska'),
		array('AL', 'Alabama'),
		array('AR', 'Arkansas'),
		array('AZ', 'Arizona'),
		array('CA', 'California'),
		array('CO', 'Colorado'),
		array('CT', 'Connecticut'),
		array('DC', 'District of Columbia'),
		array('DE', 'Delaware'),
		array('FL', 'Florida'),
		array('GA', 'Georgia'),
		array('HI', 'Hawaii'),
		array('IA', 'Iowa'),
		array('ID', 'Idaho'),
		array('IL', 'Illinois'),
		array('IN', 'Indiana'),
		array('KS', 'Kansas'),
		array('KY', 'Kentucky'),
		array('LA', 'Louisiana'),
		array('MA', 'Massachusetts'),
		array('MD', 'Maryland'),
		array('ME', 'Maine'),
		array('MI', 'Michigan'),
		array('MN', 'Minnesota'),
		array('MO', 'Missouri'),
		array('MS', 'Mississippi'),
		array('MT', 'Montana'),
		array('NC', 'North Carolina'),
		array('ND', 'North Dakota'),
		array('NE', 'Nebraska'),
		array('NH', 'New Hampshire'),
		array('NJ', 'New Jersey'),
		array('NM', 'New Mexico'),
		array('NV', 'Nevada'),
		array('NY', 'New York'),
		array('OH', 'Ohio'),
		array('OK', 'Oklahoma'),
		array('OR', 'Oregon'),
		array('PA', 'Pennsylvania'),
		array('PR', 'Puerto Rico'),
		array('RI', 'Rhode Island'),
		array('SC', 'South Carolina'),
		array('SD', 'South Dakota'),
		array('TN', 'Tennessee'),
		array('TX', 'Texas'),
		array('UT', 'Utah'),
		array('VA', 'Virginia'),
		array('VT', 'Vermont'),
		array('WA', 'Washington'),
		array('WI', 'Wisconsin'),
        array('ON', 'Ontario'),
        array('QC', 'Quebec'),
        array('NS', 'Nova Scotia'),
		array('NB', 'New Brunswick'),
		array('MB', 'Manitoba'),
		array('BC', 'British Columbia'),
		array('PE', 'Prince Edward Island'),
		array('UT', 'Saskatchewan'),
		array('AB', 'Alberta'),
		array('NL', 'Newfoundland and Labrador')
	);
	
	$options = '<option value=""></option>';
	
	foreach ($states as $state) {
		if ($type == 'abbrev') {
    	$options .= '<option value="'.$state[0].'" '. check_select($post, $state[0], false) .' >'.$state[0].'</option>'."\n";
    } elseif($type == 'name') {
    	$options .= '<option value="'.$state[1].'" '. check_select($post, $state[1], false) .' >'.$state[1].'</option>'."\n";
    } elseif($type == 'mixed') {
    	$options .= '<option value="'.$state[0].'" '. check_select($post, $state[0], false) .' >'.$state[1].'</option>'."\n";
    }
	}
		
	echo $options;
}

/**
 * Check Select Element 
 *
 * @param string $i, POST value
 * @param string $m, input element's value
 * @param string $e, return=false, echo=true 
 * @return string 
 */
function check_select($i,$m,$e=true) {
	if ($i != null) { 
		if ( $i == $m ) { 
			$var = ' selected="selected" '; 
		} else {
			$var = '';
		}
	} else {
		$var = '';	
	}
	if(!$e) {
		return $var;
	} else {
		echo $var;
	}
}
?>


<div class="container">

                <div class="col-md-9" id="checkout">

                    <div class="box">
                        <form method="post" action="entershipping">
                            <h1>Checkout</h1>
                            <h5>*required</h5>

                            <?php if(isset($errors)): ?>
                                <?php foreach($errors as $error): ?>
                                    <h6><span style= "color: red;">*<?= $error ?></span></h6>
                                <?php endforeach; ?>
                            <?php endif;?>

                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">First Name*</label>
                                            <input name="firstname" type="text" class="form-control" id="firstname" value="<?= $shippinginfo['firstname']?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Last Name*</label>
                                            <input name="lastname" type="text" class="form-control" id="lastname" value="<?= $shippinginfo['lastname']?>">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Company</label>
                                            <input name="company" type="text" class="form-control" id="company" value="<?= $shippinginfo['company']?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="street">Street Address*</label>
                                            <input name="street" type="text" class="form-control" id="street" value="<?= $shippinginfo['streetaddress']?>">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">

                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input name="city" type="text" class="form-control" id="city" value="<?= $shippinginfo['city']?>">
                                        </div>
                                    </div>
                                   
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="zip">ZIP/Postal*</label>
                                            <input name="zip" type="text" class="form-control" id="zip" value="<?= $shippinginfo['zip']?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="state">State/Province*</label>
                                            <select name="state" class="form-control" id="state"><?php echo StateDropdown($shippinginfo['state'], 'name'); ?></select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="country">Country*</label>
                                            <select name="country" class="form-control" id="country"><?php echo CountryDropdown($shippinginfo['country'], 'abbrev'); ?></select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">Telephone</label>
                                            <input name="phone" type="text" class="form-control" id="phone" value="<?= $shippinginfo['phone']?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email*</label>
                                            <input name="email" type="text" class="form-control" id="email" value="<?= $shippinginfo['email']?>">
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row -->
                            </div>

                            <div class="box-footer">
                                <div class="pull-left">
                                    
                                    <button type="submit" class="btn btn-default" name="continue" value="0"><i class="fa fa-chevron-left"></i>Back to Basket</button>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary" name="continue"  value="1">Continue to Verify<i class="fa fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class="col-md-3">

                    <!-- *** Order Summary Side Bar *** -->
                    <?= $this->element('checkout_layout/order_summary_sidebar', ['cartTotals' => $cartTotals]) ?>
                    <!-- *** END Order Summary Side Bar *** -->

                </div>
                <!-- /.col-md-3 -->

            </div>