<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsCategory[]|\Cake\Collection\CollectionInterface $itemsCategories
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Items Category'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="itemsCategories index large-9 medium-8 columns content">
    <h3><?= __('Items Categories') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('item_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($itemsCategories as $itemsCategory): ?>
            <tr>
                <td><?= $itemsCategory->has('item') ? $this->Html->link($itemsCategory->item->title, ['controller' => 'Items', 'action' => 'view', $itemsCategory->item->id]) : '' ?></td>
                <td><?= $itemsCategory->has('category') ? $this->Html->link($itemsCategory->category->title, ['controller' => 'Categories', 'action' => 'view', $itemsCategory->category->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $itemsCategory->item_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $itemsCategory->item_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $itemsCategory->item_id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemsCategory->item_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
