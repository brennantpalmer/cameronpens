<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsCategory $itemsCategory
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Items Category'), ['action' => 'edit', $itemsCategory->item_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Items Category'), ['action' => 'delete', $itemsCategory->item_id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemsCategory->item_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Items Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Items Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="itemsCategories view large-9 medium-8 columns content">
    <h3><?= h($itemsCategory->item_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Item') ?></th>
            <td><?= $itemsCategory->has('item') ? $this->Html->link($itemsCategory->item->title, ['controller' => 'Items', 'action' => 'view', $itemsCategory->item->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category') ?></th>
            <td><?= $itemsCategory->has('category') ? $this->Html->link($itemsCategory->category->title, ['controller' => 'Categories', 'action' => 'view', $itemsCategory->category->id]) : '' ?></td>
        </tr>
    </table>
</div>
