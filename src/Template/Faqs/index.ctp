<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Faq[]|\Cake\Collection\CollectionInterface $faqs
 */
?>


    <div class="box" id="contact">
        <h1>Frequently asked questions</h1>

        <p class="lead">This is list of questions that we get often. It is always best to check this page before sending us a communication as your question may already be answered.</p>
        <p>Of course we are always happy to answer any questions you may have even if they are already answered here. We are always working for you. If you would like to contact us, please head over to our <a class="standardlink" href="/communications/add">contact</a> page and shoot us a message.</p>

        <hr>

        <div class="panel-group" id="accordion">
            <?php foreach ($faqs as $faq): ?>
            <div class="panel panel-primary">
                <div class="panel-heading">

                    <?php if($adminloggedin):?>
                    
                    <h4 class="panel-title" style="color:blue">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $faq->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $faq->id]) ?>
                    </h4>
                    <h4 class="panel-title" style="color:red">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $faq->id], ['confirm' => __('Are you sure you want to delete # {0}?', $faq->id)]) . " " ?>                        
                    </h4>
                    <?php endif; ?>    

                    <h4 class="panel-title">
                    
                        <a data-toggle="collapse" data-parent="#accordion" href="#faq<?= $faq->id ?>" class="collapsed">
                                               <?= $faq->question ?> 

                        </a>

                    </h4>
                </div>
                <div id="faq<?= $faq->id ?>" class="panel-collapse collapse" style="height: 0px;">
                    <div class="panel-body">
                        <p><?= $faq->answer ?> </p>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

        </div>
        <!-- /.panel-group -->


    </div>


    
