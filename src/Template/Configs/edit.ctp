<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Config $config
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        
        <li><?= $this->Html->link(__('Cancel'), ['action' => 'view', 1]) ?></li>
    </ul>
</nav>
<div class="configs form large-9 medium-8 columns content">
    <?= $this->Form->create($config) ?>
    <fieldset>
        <legend><?= __('Edit Config') ?></legend>
        <?php
            echo $this->Form->control('standardshipping');
            echo $this->Form->control('expressshipping');
            echo $this->Form->control('twodayshipping');
            echo $this->Form->control('tax');
            echo $this->Form->control('paypalsecretkey');
            echo $this->Form->control('paypalclientid');
            echo $this->Form->control('maintenancemode');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
