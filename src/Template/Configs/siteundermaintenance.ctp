<div class="container">

                <div class="col-md-12">

                    <div class="row" id="error-page">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="box">

                                <p class="text-center">
                                    <img src="/img/banner.png" class="img-responsive" alt="Cameron Pens Logo">
                                </p>

                                <h3>We're sorry but the site is currently under maintenance. Please check back later for the best prices on the finest stationary on the web.</h3>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>