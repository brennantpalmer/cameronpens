<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Config $config
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Config'), ['action' => 'edit', $config->id]) ?> </li>
    </ul>
</nav>
<div class="configs view large-9 medium-8 columns content">
    <h3><?= h($config->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($config->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standardshipping') ?></th>
            <td><?= $this->Number->format($config->standardshipping) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expressshipping') ?></th>
            <td><?= $this->Number->format($config->expressshipping) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Twodayshipping') ?></th>
            <td><?= $this->Number->format($config->twodayshipping) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tax') ?></th>
            <td><?= $this->Number->format($config->tax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PayPal Secret Key') ?></th>
            <td><?= $config->paypalsecretkey ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PayPal Client ID') ?></th>
            <td><?= $config->paypalclientid ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Maintenance Mode Enabled') ?></th>
            <td><?= $config->maintenancemode ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($config->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($config->modified) ?></td>
        </tr>
    </table>
</div>
