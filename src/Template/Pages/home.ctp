<!-- MAIN SLIDER -->
<div class="container">
    <div class="col-md-12">
        <div id="main-slider">
            <div class="item">
                <img src="img/main-slider1.jpg" alt="" class="img-responsive">
            </div>
        </div>
        <!-- /#main-slider -->
    </div>
</div>

<!-- *** ADVANTAGES HOMEPAGE *** -->
<div id="advantages">

    <div class="container">
    <div class = "home-advantages">
        <div class="same-height-row">
            <div class="col-sm-4">
                
                <div class="box same-height">
                    <div class="icon">
                        <i class="fa fa-heart"></i>
                    </div>

                    <h3>
                        <span style= "color: #000000;">We love our customers</span>
                    </h3>
                    <p>We are known to provide best possible service ever</p>
                </div>
                
            </div>

            <div class="col-sm-4">
                <div class="box same-height">
                    <div class="icon">
                        <i class="fa fa-tags"></i>
                    </div>

                    <h3>
                    
                        <span style= "color: #000000;">Best prices</span>
                    </h3>
                    <p>We have the finest quality products at the best prices.</p>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="box same-height">
                    <div class="icon">
                        <i class="fa fa-thumbs-up"></i>
                    </div>

                    <h3>
                        <span style= "color: #000000;">100% satisfaction guaranteed</span>
                    </h3>
                    <p>Free returns on everything for 3 months.</p>
                </div>
            </div>
        </div>
        <!-- /.row -->
        </div>
    </div>
    <!-- /.container -->

</div>
<!-- /#advantages -->

<!-- *** ADVANTAGES END *** -->

<!-- *** FEATURED PRODUCT SLIDESHOW *** -->

<div id="hot">

    <div class="box">
        <div class="container">
            <div class="col-md-12">
                <h2>Featured This Week</h2>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="product-slider">

            <?php foreach ($featuredItems as $item): ?>

                <div class="item">
                    <!-- *** Item *** -->
                        <?= $this->element('item_homepage', ['item' => $item]) ?>
                    <!-- *** Item END *** -->
                </div>

            <?php endforeach; ?>

        </div>
        <!-- /.product-slider -->
    </div>
    <!-- /.container -->

</div>
<!-- /#hot -->

<!-- *** HOT END *** -->

<!-- *** ON SALE PRODUCT SLIDESHOW *** -->
<div id="hot">

    <div class="box">
        <div class="container">
            <div class="col-md-12">
                <h2>On Sale This week</h2>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="product-slider">
            <?php foreach ($onSaleItems as $item): ?>

                <div class="item">
                    <!-- *** Item *** -->
                        <?= $this->element('item_homepage', ['item' => $item]) ?>
                    <!-- *** Item END *** -->
                </div>

            <?php endforeach; ?>
        </div>
        <!-- /.product-slider -->
    </div>
    <!-- /.container -->

</div>
<!-- /#hot -->

<!-- *** ON SALE END *** -->



<!-- *** GET INSPIRED *** -->
<div class="container" data-animate="fadeInUpBig">
    <div class="col-md-12">
        <div class="box slideshow">
            <h3>Treat Yourself!</h3>
            <div id="get-inspired" class="owl-carousel owl-theme">
                <div class="item">
                    <a href="/items">
                        <img src="img/TreatYourself.jpg" alt="Treat Yourself!" class="img-responsive">
                    </a>
                </div>
                <div class="item">
                    <a href="/materials">
                        <img src="img/Slider2.jpg" alt="We use the finest materials" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- *** GET INSPIRED END *** -->

