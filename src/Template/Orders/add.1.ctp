<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Orderitems'), ['controller' => 'Orderitems', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Orderitem'), ['controller' => 'Orderitems', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Add Order') ?></legend>
        <?php
            echo $this->Form->control('cptransactionid');
            echo $this->Form->control('paypaltransactionid');
            echo $this->Form->control('paypalpayerid');
            echo $this->Form->control('description');
            echo $this->Form->control('firstname');
            echo $this->Form->control('lastname');
            echo $this->Form->control('streetaddress');
            echo $this->Form->control('company');
            echo $this->Form->control('city');
            echo $this->Form->control('state');
            echo $this->Form->control('zip');
            echo $this->Form->control('email');
            echo $this->Form->control('phone');
            echo $this->Form->control('tax');
            echo $this->Form->control('subtotal');
            echo $this->Form->control('total');
            echo $this->Form->control('completed');
            echo $this->Form->control('cancelled');
            echo $this->Form->control('shipped');
            echo $this->Form->control('orderitems._ids', ['options' => $orderitems]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
