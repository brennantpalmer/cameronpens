<div class="container">

    <div class="col-md-12">

        <div class="row" id="error-page">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="box" id="invoice">

                    <p class="text-center">
                        <img src="/img/banner.png" class="img-responsive" alt="Obaju template">
                    </p>

                    <h3>Thank you for your purchase!</h3>
                    <h5 class="text-muted">Please print this page for your records</h5>
                    <br>

                    <h5 class="text">
                        <span style="color: black; font-size:20px; font-weight: bold;">Ship To:</span>
                    </h5>

                    <h4 class="text-center">
                        <span class="text">
                            <?= $order->firstname . ' ' . $order->lastname ?>
                        </span>
                        <br>
                        <span class="text">
                            <?= $order->streetaddress ?>
                        </span>
                        <br>
                        <span class="text">
                            <?= $order->city . ', ' . $order->state . ' ' . $order->zip . '      ' . $order->country ?>
                        </span>
                    </h4>
                    

                    <h5 class="text">
                        <span style="color: black; font-size:20px; font-weight: bold;">Items:</span>
                    </h5>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Price</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($order->orderitems as $item): ?>

                                <tr>
                                    <td>
                                        <?= $item->title ?>
                                    </td>
                                    <td>
                                        <?= $item->itemid ?>
                                    </td>
                                    <td>
                                        <?= $item->quantity ?>
                                    </td>
                                    <td>
                                        $
                                        <?= number_format((float)($item->price * $item->quantity), 2, '.', ''); ?>
                                    </td>

                                </tr>


                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>

                    <br>

                    <h5 class="text">
                        <span style="color: black; font-size:20px; font-weight: bold;">Details:</span>
                    </h5>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        Invoice Number:
                                    </td>
                                    <td>
                                        <?= $order->cptransactionid ?>
                                    </td>
                                </tr>  
                                <tr>
                                    <td>
                                        Transaction ID:
                                    </td>
                                    <td>
                                        <?= $order->paypaltransactionid ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date:
                                    </td>
                                    <td>
                                        <?= $order->created ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Customer Email:
                                    </td>
                                    <td>
                                        <?= $order->email ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Subtotal:
                                    </td>
                                    <td>
                                        $
                                        <?= number_format((float)($order->subtotal), 2, '.', ''); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tax:
                                    </td>
                                    <td>
                                        $
                                        <?= number_format((float)($order->tax), 2, '.', ''); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Shipping:
                                    </td>
                                    <td>
                                        $
                                        <?= number_format((float)($order->shipping), 2, '.', ''); ?>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <span style="color: black; font-size:20px; font-weight: bold;">
                                            Total:
                                        </span>
                                    </td>
                                    <td>
                                        <span style="color: black; font-size:20px; font-weight: bold;">
                                            $
                                            <?= number_format((float)($order->total), 2, '.', ''); ?>

                                        </span>
                                    </td>

                                </tr>
                            </tbody>

                        </table>

                    </div>

                    <p class="buttons">
                        <a href="/" class="btn btn-primary">
                            <i class="fa fa-home"></i> Go to Homepage</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-md-9 -->
</div>

