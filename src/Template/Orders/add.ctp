<div class="container">

    <div class="col-md-9" id="checkout">

        <div class="box">
            <form method="post" action="/orders/add">
                <h1>Review Order</h1><br>

                
                <div class="box" id="verifyshipping">
                <h3>Shipping Address:</h3>
                <h4>
            <?= $shippinginfo['firstname']; ?> <?= $shippinginfo['lastname']; ?> <br>
            <?= $shippinginfo['streetaddress']; ?> <br>
            <?= $shippinginfo['city']; ?>, <?= $shippinginfo['state']; ?> <?= $shippinginfo['zip']; ?> <br>
            <?= $shippinginfo['country']; ?> <br>    
            
            </h4>
</div>

                <h6>*Note: You will be redirected to paypal to complete the transaction</h6>
                <div class="content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="2">Product</th>
                                    <th>Quantity</th>
                                    <th>Unit price</th>
                                    <th>Discount</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($Cart as $item): ?>

                                <tr>
                                    <td>
                                        <a href="#">
                                            <?= $this->Html->link(($this->Html->image('/files/Photos/title/detailsmall-' . $item['item']['photos'][0]['title'], ['alt' => $item['item']['photos'][0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item['item']['id']],['escape' => false]) ?>
                                        </a>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(__(h($item['item']['title'])), ['action' => 'view', $item['item']['id']]) ?>
                                    </td>
                                    <td>
                                        <?= $this->Number->format($item['quantity']) ?>
                                    </td>
                                    <td>
                                        <?= '$' . h($item['item']['standardprice']) ?>
                                    </td>
                                    <td>
                                        <?= '$' . $item['discount'] ?>
                                    </td>
                                    <td>
                                        <?= '$' . $item['total'] ?>
                                    </td>
                                </tr>


                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5"><span style= "font-size: 15px;" >Subtotal</span></td>
                                <td colspan="2"><span style= "font-size: 15px;" >$
                                    <?=$cartTotals['subtotal']?></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><span style= "font-size: 15px;" >Shipping & Handling</span></td>
                                <td colspan="2"><span style= "font-size: 15px;" >$
                                    <?=$cartTotals['shipping']?></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><span style= "font-size: 15px;" >Tax</span></td>
                                <td colspan="2"><span style= "font-size: 15px;" >$
                                    <?=$cartTotals['tax']?></span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="5">Total</th>
                                <th colspan="2">$
                                    <?=$cartTotals['total']?>
                                </th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.content -->

                <div class="box-footer">
                    <div class="pull-left">
                        <button type="submit" class="btn btn-default" name="continue" value="0"><i class="fa fa-chevron-left"></i>Back to Shipping</button>
                    </div>
                    <div class="pull-right">
                    <button type="submit" class="btn btn-primary" name="continue"  value="1">Checkout with Paypal<i class="fa fa-chevron-right"></i>
                                    </button>
                    </div>
                    
                </div>
                <div class="pull-left" id="paypallogo">
                    <?= $this->element('paypallogo') ?>
                </div>
                <div class="pull-right" id="paypalcards">
                    <?= $this->element('paypalcards') ?>
                </div>
            </form>
        </div>
        <!-- /.box -->


    </div>
    <!-- /.col-md-9 -->

    <div class="col-md-3">

        <!-- *** Order Summary Side Bar *** -->
        <?= $this->element('checkout_layout/order_verify_sidebar', ['cartTotals' => $cartTotals]) ?>
                    <!-- *** END Order Summary Side Bar *** -->
    </div>
    <!-- /.col-md-3 -->

</div>
