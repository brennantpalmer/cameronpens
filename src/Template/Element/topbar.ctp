<div id="top">
    <div class="container">
        <div class="col-md-6 offer" data-animate="fadeInDown">
        This site uses cookies, by using this service you agree to our <a class="standardlink" href="/TermsAndConditions">Term and Conditions</a>
        </div>
        <div class="col-md-6" data-animate="fadeInDown">
            <ul class="menu">
                <li>
                    <a href="/about">About</a>
                </li>
                <li>
                    <a href="/communications/add">Contact</a>
                </li>
                <li>
                    <a href="/faqs">FAQ</a>
                </li>
            </ul>
        </div>
    </div>
</div>

