<div id="footer" data-animate="fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h4>Pages</h4>

                <ul>
                    <li>
                        <a href="/about">About us</a>
                    </li>
                    <li>
                        <a href="/TermsAndConditions">Terms and conditions</a>
                    </li>
                    <li>
                        <a href="/PrivacyPolicy">Privacy policy</a>
                    </li>
                    <li>
                        <a href="/faqs">FAQ</a>
                    </li>
                    <li>
                        <a href="/communications/add">Contact us</a>
                    </li>
                </ul>
            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Top categories</h4>

                <ul>
                    <li>
                        <a href="/items/bundles">Bundles</a>
                    </li>
                    <li>
                        <a href="/items/onsale">On Sale</a>
                    </li>
                    <li>
                        <a href="/items/featured">Featured</a>
                    </li>
                    <li>
                        <a href="/materials">Browse Materials</a>
                    </li>
                </ul>

                <h5>Other Cool Stuff</h5>
                <ul>
                <?php 
                    $counter = 0;
                    $sorted = $global_categories->sortBy('id', SORT_DESC);
                ?>
                    <?php foreach ($sorted as $singleCategory): ?>

                   
                    <li>
                        <a href="/categories/view/<?= $singleCategory->id ?>">
                            <?= $singleCategory->title ?>
                        </a>
                    </li>

                    <?php $counter++; if($counter > 4){ break;} ?>
                    <?php endforeach; ?>
                </ul>

                <hr class="hidden-md hidden-lg">

            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Where to find us</h4>

                <p>
                    <strong>Cameron Pens</strong>
                    <br>Broken Arrow 74011
                    <br>Oklahoma
                    <br>
                    <strong>United States</strong>
                </p>

                <div class="standardlink">
                <a class="standardlink" href="/communications/add">Go to contact page</a>
                </div>

                <hr class="hidden-md hidden-lg">

            </div>
            <!-- /.col-md-3 -->



            <div class="col-md-3 col-sm-6">
                <hr>

                <h4>Stay in touch</h4>

                <p class="social">
                    <a href="#" class="facebook external" data-animate-hover="shake">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="twitter external" data-animate-hover="shake">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="instagram external" data-animate-hover="shake">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="#" class="gplus external" data-animate-hover="shake">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a href="#" class="email external" data-animate-hover="shake">
                        <i class="fa fa-envelope"></i>
                    </a>
                </p>


            </div>
            <!-- /.col-md-3 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>
<!-- /#footer -->
