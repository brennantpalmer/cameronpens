<?php

   
        $photos = array_values($item->photos);
   
   
    $photoPathOne = '/files/Photos/title/browse-';
    $photoPathTwo = '/files/Photos/title/browse-';    
    if(!isset($photos[0]['title'])){$photos[0]['title'] = 'banner.png'; $photoPathOne = '/img/';}
    if(!isset($photos[1]['title'])){$photos[1]['title'] = 'banner.png'; $photoPathTwo = '/img/';}
    ?>

<div class="product">
                <div class="flip-container">
                    <div class="flipper">
                        <div class="front">
                            <div class="itemIndexPic">
                            <?= $this->Html->link(($this->Html->image($photoPathOne . $photos[0]['title'], ['alt' => $photos[0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false]) ?>
</div>
                        </div>
                        <div class="back">
                        <div class="itemIndexPic">
                            <?= $this->Html->link(($this->Html->image($photoPathTwo . $photos[1]['title'], ['alt' => $photos[1]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false]) ?>
</div>
                        </div>
                    </div>
                </div>
                <?= $this->Html->link(($this->Html->image('/files/Photos/title/browse-' . $photos[0]['title'], ['alt' => $photos[0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false, 'class' => 'invisible']) ?>
                
                
                <?php
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** START ONSALE CONDITION ***////////////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>

                    <?php if ($item->onsale == 1): ?>
                    <div class="text">
                        <h3>
                            <?= $this->Html->link(__(h($item->title)), ['action' => 'view', $item->id]) ?>
                            
                        </h3>
                        <div class="itemPictureSeperator">
                <hr>
                </div>    
                        <p class="price">
                        
                            <del>$<?= h($item->standardprice) ?></del> $<?= h($item->actualprice) ?>
                        </p>
                        <div class ="text-center">
                        <p>
                            <?php foreach($item->materials as $m) :?>
                            <?= $m->title . " " ?>
                            <?php endforeach;?>
                        </p>
                        </div>
                    </div>


                    <?php
                        // Calcualte the percentage off for display
                        $percentageoff = ($item->standardprice - $item->saleprice)/$item->standardprice;
                        $percentageoff *= 100;
                        $percentageoff = round($percentageoff, 0, PHP_ROUND_HALF_DOWN);
                        if($percentageoff === 100.)
                        {
                            $percentageoff = 99;
                        }
                        ?>

                        <!-- /.text -->
                        <div class="ribbon sale">
                            <div class="theribbon">
                                On Sale! <?= h($percentageoff)?>% Off</div>
                            <div class="ribbon-background"></div>
                        </div>
                        <!-- /.ribbon -->

                        <?php
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** END ONSALE CONDITION ***//////////////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>



                            <?php 
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** BEGIN NOT ON SALE CONDITION ***///////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>
                            <?php else:?>

                            <div class="text">
                                <h3>
                                <?= $this->Html->link(__(h($item->title)), ['action' => 'view', $item->id]) ?>
                                
                            </h3>
                            <div class="itemPictureSeperator">
                <hr>
                </div>    
                                <p class="price">
                                
                                $<?= h($item->actualprice) ?>
                                </p>
                                <div class ="text-center">
                        <p>
                            <?php foreach($item->materials as $m) :?>
                            <?= $m->title . " " ?>
                            <?php endforeach;?>
                        </p>
                        </div>
                            </div>


                            <?php 
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** END NOT ON SALE CONDITION ***/////////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>
                            <?php endif; ?>

                        <?php if($adminloggedin):?>
                            
                            <div class="admin-action-links">
                                <?= $this->Html->link(__('View'), ['action' => 'view', $item->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $item->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?>
                            </div>

                            <?php if(!$item->active):?>
                                <div class="admin-item-inactive">
                                    NOT ACTIVE
                                </div>
                            <?php endif;?>

                            <?php if($item->active):?>
                                <div class="admin-item-active">
                                    ACTIVE
                                </div>
                            <?php endif;?>

                        <?php endif;?>

            </div>
            <!-- /.product -->