<div class="col-md-3">
    <div class="panel panel-default sidebar-menu">

        <div class="panel-heading">
            <h3 class="panel-title">Filter</h3>
        </div>

        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked category-menu">
                <li>
                    <a href="/items/featured">Featured
                        
                    </a>
                    <ul>
                        <li>
                            <a href="/items/hot">Hot Items</a>
                        </li>
                        <li>
                            <a href="/items/favorite">Our Favorites</a>
                        </li>
                        <li>
                            <a href="/items/bundles">Bundles</a>
                        </li>
                    </ul>
                </li>
                <li class="nav nav-pills nav-stacked category-menu">
                    <a href="/categories">Categories
                       
                    </a>
                    <ul>
                        <?php foreach ($global_categories as $single_category): ?>
                        <li>
                            <a href="/categories/view/<?= $single_category->id ?>">
                                <?= $single_category->title ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>

        </div>
    </div>

    <!-- *** MENUS AND FILTERS END *** -->
    <div class="banner">
        
            <img src="/img/banner.png" alt="Cameron Pens Logo" class="img-responsive">
        
    </div>
</div>
