<div class="navbar navbar-default yamm" role="navigation" id="navbar">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand home" href="/">
                <img src="/img/logo.png" alt="Cameronpens logo" class="hidden-xs">
                <img src="/img/logo-small.png" alt="Cameronpens logo" class="visible-xs">
                <span class="sr-only">Cameron Pens - go to homepage</span>
            </a>
            <div class="navbar-buttons">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-align-justify"></i>
                </button>

                
                
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                    <span class="sr-only">Toggle search</span>
                    <i class="fa fa-search"></i>
                </button>
                
                
                <a class="btn btn-default navbar-toggle" href="/items/cart">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="hidden-xs">
                        <?= $cartCount ?> items in cart</span>
                </a>
            </div>
        </div>
        <!--/.navbar-header -->

        <div class="navbar-collapse collapse" id="navigation">

            <ul class="nav navbar-nav navbar-left">
                <!-- Home Link -->
                <li class="">
                    <a href="/">Home</a>
                </li>
                <!-- Shop All Link -->
                <li class="">
                    <a href="/items">Shop All</a>
                </li>
                <!-- Explore Dropdown -->
                <li class="dropdown yamm-fw">
                    <a href="/items" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Explore
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h5>Categories</h5>
                                        <ul>


                                            <?php $counter = 0; ?>
                                            <?php foreach ($global_categories as $singleCategory): ?>

                                            <li>
                                                <a href="/categories/view/<?= $singleCategory->id ?>">
                                                    <?= $singleCategory->title ?>
                                                </a>
                                            </li>

                                            <?php $counter++; if($counter > 4){ break;} ?>
                                            <?php endforeach; ?>

                                            <li>
                                                <a href="/categories">More</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Materials</h5>
                                        <ul>
                                            <?php $counter = 0;?>
                                            <?php if($global_materials) : ?>
                                            <?php foreach ($global_materials as $singleMaterial): ?>

                                            <li>
                                                <a href="/materials/view/<?= isset($singleMaterial->id) ? $singleMaterial->id : ''?>">
                                                    <?= $singleMaterial->title ?>
                                                </a>
                                            </li>

                                            <?php $counter++; if($counter > 4){break;} ?>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                            <li>
                                                <a href="/materials">More</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Featured</h5>
                                        <ul>
                                            <li>
                                                <a href="/items/bundles">Bundles</a>
                                            </li>
                                            <li>
                                                <a href="/items/hot">Hot This Week</a>
                                            </li>
                                            <li>
                                                <a href="/items/favorite">Our Favorites</a>
                                            </li>
                                        </ul>
                                        <h5>Want a Custom Pen?</h5>
                                        <ul>
                                            <li>
                                                <a href="/communications/add">Contact Us</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="banner">
                                            
                                                <img src="/img/banner.png" class="img img-responsive" alt="">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.yamm-content -->
                        </li>
                    </ul>
                </li>

                <?php if($adminloggedin):?>
                <!-- ADMIN Dropdown -->
                <li class="dropdown yamm-fw">
                    <a href="/items" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">ADMIN
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h5>Items</h5>
                                        <ul>
                                            <li>
                                                <a href="/items/add">Add New</a>
                                            </li>
                                            <li>
                                                <a href="/items">List</a>
                                            </li>
                                        </ul>
                                        <h5>Photos</h5>
                                        <ul>
                                            <li>
                                                <a href="/photos/add">Add New</a>
                                            </li>
                                            <li>
                                                <a href="/photos">List</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Categories</h5>
                                        <ul>
                                            <li>
                                                <a href="/categories/add">Add New</a>
                                            </li>
                                            <li>
                                                <a href="/categories">List</a>
                                            </li>
                                        </ul>
                                        <h5>Communications</h5>
                                        <ul>
                                            <li>
                                                <a href="/communications">List</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Materials</h5>
                                        <ul>
                                            <li>
                                                <a href="/materials/add">Add New</a>
                                            </li>
                                            <li>
                                                <a href="/materials">List</a>
                                            </li>
                                        </ul>
                                        <h5>Orders</h5>
                                        <ul>
                                            <li>
                                                <a href="/orders">List</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Tags</h5>
                                        <ul>
                                            <li>
                                                <a href="/tags/add">Add New</a>
                                            </li>
                                            <li>
                                                <a href="/tags">List</a>
                                            </li>
                                        </ul>
                                        <h5>FAQ</h5>
                                        <ul>
                                            <li>
                                                <a href="/faqs/add">Add New</a>
                                            </li>
                                            <li>
                                                <a href="/faqs">List</a>
                                            </li>
                                        </ul>
                                        <h5>Config</h5>
                                        <ul>
                                            <li>
                                                <a href="/configs/view/1">Show</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.yamm-content -->
                        </li>
                    </ul>
                </li>

                <!-- ADMIN Logout -->
                <li class="">
                    <a href="/admins/logout">Logout</a>
                </li>
                <?php endif;?>

            </ul>

        </div>
        <!--/.nav-collapse -->

        <div class="navbar-buttons">

            <div class="navbar-collapse collapse right" id="basket-overview">
                <a href="/items/cart" class="btn btn-primary navbar-btn">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="hidden-sm" id="cart-counter">
                        <?= $cartCount ?> items in cart</span>
                </a>
            </div>
            <!--/.nav-collapse -->
    
            <div class="navbar-collapse collapse right" id="search-not-mobile">
                <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                    <span class="sr-only">Toggle search</span>
                    <i class="fa fa-search"></i>
                </button>
            </div>
            

        </div>

        
        <div class="collapse clearfix" id="search">

                <?php $this->Form->templates(['inputContainer' => '{{content}}']);?>
                <div class="navbar-form">
                    <?= $this->Form->create(null, ['url' => ['controller' => 'Items', 'action' => 'index', 'class' => 'navbar-form']], ['valueSources' => 'query'], ['class' => 'navbar-form', 'role' => 'search']) ?>
                    
                        <div class="input-group">
                                
                            <?= $this->Form->control('q', ['label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Search']) ?>
                            <span class="input-group-btn">      
                                <?= $this->Form->button('<i class="fa fa-search"></i>', ['type' => 'submit', 'class' => 'btn btn-primary']) ?>
                            </span>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
        </div>
        <!--/.nav-collapse -->
        
    </div>
    <!-- /.container -->
</div>
<!-- /#navbar -->
