<div class="col-md-3">
    <!-- *** PAGES MENU ***
 _________________________________________________________ -->
    <div class="panel panel-default sidebar-menu">

        <div class="panel-heading">
            <h3 class="panel-title">Pages</h3>
        </div>

        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/about">About</a>
                </li>
                <li>
                    <a href="/communications/add">Contact Us</a>
                </li>
                <li>
                    <a href="/faqs">FAQ</a>
                </li>
                <li>
                    <a href="/TermsAndConditions">Terms and Conditions</a>
                </li>
                <li>
                    <a href="/PrivacyPolicy">Privacy Policy</a>
                </li>


            </ul>

        </div>
    </div>

    <!-- *** PAGES MENU END *** -->


    <div class="banner">
        <a href="#">
            <img src="/img/banner.png" alt="Cameron Pens Logo" class="img-responsive">
        </a>
    </div>
</div>
