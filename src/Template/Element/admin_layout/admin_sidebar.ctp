<div class="col-md-3">
    <!-- *** PAGES MENU ***
 _________________________________________________________ -->
    <div class="panel panel-default sidebar-menu">

        <div class="panel-heading">
            <h3 class="panel-title">ADMIN</h3>
        </div>

        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="/items">Items</a>
                </li>
                <li>
                    <a href="/materials">Materials</a>
                </li>
                <li>
                    <a href="/categories">Categories</a>
                </li>
                <li>
                    <a href="/faqs">FAQs</a>
                </li>
                <li>
                    <a href="/Photos">Photos</a>
                </li>
                <li>
                    <a href="/tags">Tags</a>
                </li>
                <li>
                    <a href="/orders">Orders</a>
                </li>
                <li>
                    <a href="/configs/view/1">Settings</a>
                </li>



            </ul>

        </div>
    </div>

    <!-- *** PAGES MENU END *** -->


    <div class="banner">
        <a href="#">
            <img src="/img/banner.png" alt="Cameron Pens Logo" class="img-responsive">
        </a>
    </div>
</div>
