<?php
                    $photos = array_values($item->photos);
                    if(!isset($photos[0]))
                    {
                        $photos[0]['title'] = 'logo.png';
                    }
                
                    if(!isset($photos[1]))
                    {
                        $photos[1]['title'] = 'logo.png';
                    }
                    ?>

<div class="product">
    <div class="flip-container">
        <div class="flipper">
            <div class="front">
                <?= $this->Html->link(($this->Html->image('/files/Photos/title/browse-' . $photos[0]['title'], ['alt' => $photos[0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false]) ?>
            </div>
            <div class="back">
                <?= $this->Html->link(($this->Html->image('/files/Photos/title/browse-' . $photos[1]['title'], ['alt' => $photos[1]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false]) ?>
            </div>
        </div>
    </div>
    <?= $this->Html->link(($this->Html->image('/files/Photos/title/browse-' . $photos[0]['title'], ['alt' => $photos[0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false, 'class' => 'invisible']) ?>

            <?php if ($item->onsale == 1): ?>
            <div class="text">
                <h3>
                    <?= $this->Html->link(__(h($item->title)), ['controller' => 'items', 'action' => 'view', $item->id]) ?>

                </h3>
                <p class="price">
                    <del>$
                        <?= h($item->standardprice) ?>
                    </del> $
                    <?= h($item->saleprice) ?>
                </p>
            </div>


            <?php
                                // Calcualte the percentage off for display
                                $percentageoff = ($item->standardprice - $item->saleprice)/$item->standardprice;
                                $percentageoff *= 100;
                                ?>

                <!-- /.text -->
                <div class="ribbon sale">
                    <div class="theribbon">-
                        <?= h(round($percentageoff))?>%</div>
                    <div class="ribbon-background"></div>
                </div>
                <!-- /.ribbon -->
                    <?php else:?>

                    <div class="text">
                        <h3>
                            <?= $this->Html->link(__(h($item->title)), ['controller' => 'items', 'action' => 'view', $item->id]) ?>
                        </h3>
                        <p class="price">$
                            <?= h($item->standardprice) ?>
                        </p>
                    </div>


                    <?php 
                                    ////////////////////////////////////////////////////////////////////////
                                    //////////////////////*** END NOT ON SALE CONDITION ***/////////////////
                                    ////////////////////////////////////////////////////////////////////////
                                ?>
                    <?php endif; ?>

</div>
<!-- /.product -->
