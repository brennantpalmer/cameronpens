<?php

    $photos = array_values($item->photos);
    $photoPath = '/files/Photos/title/browse-';
    if(!isset($photos[0]['title'])){$photos[0]['title'] = 'logo.png'; $photoPath = '/img/';}
    if(!isset($photos[1]['title'])){$photos[1]['title'] = 'logo.png';}
    ?>

<div class ="view">
<div class="product">
                <div class="flip-container">
                    <div class="flipper">
                        <div class="front">
                            <?= $this->Html->link(($this->Html->image($photoPath . $photos[0]['title'], ['alt' => $photos[0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false]) ?>
                        </div>
                        <div class="back">
                            <?= $this->Html->link(($this->Html->image($photoPath . $photos[1]['title'], ['alt' => $photos[1]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false]) ?>
                        </div>
                    </div>
                </div>
                <?= $this->Html->link(($this->Html->image('/files/Photos/title/browse-' . $photos[0]['title'], ['alt' => $photos[0]['title'], 'class'=>'img-responsive'])), ['controller'=>'items','action' => 'view', $item->id],['escape' => false, 'class' => 'invisible']) ?>
                <?php
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** START ONSALE CONDITION ***////////////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>

                    <?php if ($item->onsale == 1): ?>
                    <div class="text">
                        <h3>
                            <?= $this->Html->link(__(h($item->title)), ['action' => 'view', $item->id]) ?>
                        </h3>
                        <p class="price">
                            <del>$<?= h($item->standardprice) ?></del> $<?= h($item->actualprice) ?>
                        </p>
                        <div class ="text-center">
                        <p class="buttons">
                            <?= $this->Html->link(__("View"), ['action' => 'view', $item->id], ['class' => "btn btn-default"]) ?>
                        </p>
                        </div>
                    </div>


                    <?php
                        // Calcualte the percentage off for display
                        $percentageoff = ($item->standardprice - $item->saleprice)/$item->standardprice;
                        $percentageoff *= 100;
                        $percentageoff = round($percentageoff, 0, PHP_ROUND_HALF_DOWN);
                        if($percentageoff === 100.)
                        {
                            $percentageoff = 99;
                        }
                        ?>

                        <!-- /.text -->
                        <div class="ribbon sale">
                            <div class="theribbon">
                                On Sale! <?= h($percentageoff)?>% Off</div>
                            <div class="ribbon-background"></div>
                        </div>
                        <!-- /.ribbon -->

                        <?php
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** END ONSALE CONDITION ***//////////////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>



                            <?php 
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** BEGIN NOT ON SALE CONDITION ***///////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>
                            <?php else:?>

                            <div class="text">
                                <h3>
                                <?= $this->Html->link(__(h($item->title)), ['action' => 'view', $item->id]) ?>
                                </h3>
                                <p class="price">
                                $<?= h($item->actualprice) ?>
                                </p>
                                <div class ="text-center">
                        <p class="buttons">
                            <?= $this->Html->link(__("View"), ['action' => 'view', $item->id], ['class' => "btn btn-default"]) ?>
                                
                        </p>
                        </div>
                            </div>


                            <?php 
                            ////////////////////////////////////////////////////////////////////////
                            //////////////////////*** END NOT ON SALE CONDITION ***/////////////////
                            ////////////////////////////////////////////////////////////////////////
                        ?>
                            <?php endif; ?>




            </div>
            <!-- /.product -->
            </div>