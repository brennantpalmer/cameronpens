<div class="box" id="order-summary">
    <div class="box-header">
        <h3>Order summary</h3>
    </div>
    <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>

    

        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Order subtotal</td>
                        <th>$<?= $cartTotals['subtotal'] ?>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            S + H
                            <?php if($cartCount > 0) : ?>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">Options
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li <?php if($shippingselection==='standard' ){ echo 'class="active"';} ?>>
                                        <?= $this->Form->postLink(__('Standard'), ['action' => 'setshippingstandard']) ?>
                                    </li>
                                    <li <?php if($shippingselection==='express' ){ echo 'class="active"';} ?>>
                                        <?= $this->Form->postLink(__('Express'), ['action' => 'setshippingexpress']) ?>
                                    </li>
                                    <li <?php if($shippingselection==='twoday' ){ echo 'class="active"';} ?>>
                                        <?= $this->Form->postLink(__('Two Day'), ['action' => 'setshippingtwoday']) ?>
                                    </li>
                                </ul>
                            </div>
                            <?php endif; ?>
                        </td>
                        <th>$<?= $cartTotals['shipping'] ?>
                        </th>
                    </tr>
                    <tr>
                        <td>Tax</td>
                        <th>$<?= $cartTotals['tax'] ?>
                        </th>
                    </tr>
                    <tr class="total">
                        <td>Total</td>
                        <th>$<?= $cartTotals['total'] ?>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>

</div>
