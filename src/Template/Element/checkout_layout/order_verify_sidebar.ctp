<div class="box" id="order-summary">
    <div class="box-header">
        <h3>Order summary</h3>
    </div>
    <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>

    

        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Order subtotal</td>
                        <th>$<?= $cartTotals['subtotal'] ?>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            S + H
                            
                        </td>
                        <th>$<?= $cartTotals['shipping'] ?>
                        </th>
                    </tr>
                    <tr>
                        <td>Tax</td>
                        <th>$<?= $cartTotals['tax'] ?>
                        </th>
                    </tr>
                    <tr class="total">
                        <td>Total</td>
                        <th>$<?= $cartTotals['total'] ?>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>

</div>
