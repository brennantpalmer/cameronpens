<?= $this->Html->charset() ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>
    <?= $cameronpensDescription ?>:
    <?= $this->fetch('title') ?>
</title>
<?= $this->Html->meta('icon') ?>
<?= $this->Html->css('font-awesome.css') ?>
<?= $this->Html->css('bootstrap.min.css') ?>
<?= $this->Html->css('animate.min.css') ?>
<?= $this->Html->css('owl.carousel.css') ?>
<?= $this->Html->css('owl.theme.css') ?>
<?= $this->Html->css('style.default.css') ?>
<?= $this->Html->css('custom.css') ?>
<?= $this->Html->css('item-index.css') ?>

<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

<?= $this->Html->script('respond.min.js') ?>

<?= $this->fetch('meta') ?>
<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>

<link rel="shortcut icon" href="favicon.png">