<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Communication $communication
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Communication'), ['action' => 'edit', $communication->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Communication'), ['action' => 'delete', $communication->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communication->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Communications'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Communication'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="communications view large-9 medium-8 columns content">
    <h3><?= h($communication->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Firstname') ?></th>
            <td><?= h($communication->firstname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lastname') ?></th>
            <td><?= h($communication->lastname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($communication->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subject') ?></th>
            <td><?= h($communication->subject) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($communication->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($communication->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($communication->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Message') ?></h4>
        <?= $this->Text->autoParagraph(h($communication->message)); ?>
    </div>
</div>
