<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Communication $communication
 */
?>

<div class="box" id="contact">
                        <h1>Contact</h1>

                        <p class="lead">Are you curious about something? Do you have some feedback you would like to give us? Need to report a problem with our site?</p>
                        <p>Please feel free to contact us, we are always working for you. The easiest way is to enter your information below and click send.</p>

                        <hr>

                        <div class="row">
                            <div class="col-sm-4">
                                <h3><i class="fa fa-map-marker"></i> Address</h3>
                                <p>1234 Elm Ave
                                    <br>Broken Arrow
                                    <br>74136
                                    <br>Oklahoma
                                    <br>
                                    <strong>United States</strong>
                                </p>
                            </div>
                            <!-- /.col-sm-4 -->
                            <div class="col-sm-4">
                                <h3><i class="fa fa-phone"></i>Call</h3>
                                <p class="text-muted">This number is toll free if calling from the United States.</p>
                                <p><strong>555 555 5555</strong>
                                </p>
                            </div>
                            <!-- /.col-sm-4 -->
                            <div class="col-sm-4">
                                <h3><i class="fa fa-envelope"></i> Electronic support</h3>
                                <p class="text-muted">Please feel free to shoot us an email.</p>
                                <ul>
                                    <li><strong><a href="mailto:">info@fakeemail.com</a></strong>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.col-sm-4 -->
                        </div>
                        <!-- /.row -->

                        <hr>

                        <h2>Contact form</h2>

                        <?= $this->Form->create($communication) ?>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="firstname">Firstname</label>
                                        <?=  $this->Form->control('firstname', array( 'label' => false, 'class' => "form-control" )) ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="lastname">Lastname</label>
                                        <?= $this->Form->control('lastname', array( 'label' => false , 'class' => "form-control" )) ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <?= $this->Form->control('email', array( 'label' => false , 'class' => "form-control" )) ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="subject">Subject</label>
                                        <?= $this->Form->control('subject', array( 'label' => false , 'class' => "form-control" )) ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <?= $this->Form->control('message', array( 'label' => false , 'class' => "form-control" )) ?>
                                    </div>
                                </div>
                                </fieldset>
                                <?= $this->Form->button(__('<i class="fa fa-envelope-o"></i> Send message'), array('class'=>"btn btn-primary")) ?>
                                <?= " " . $successmessage ?>
                            </div>
                            <!-- /.row -->
                            
    
    <?= $this->Form->end() ?>


                    </div>


    
      
        
            
            
            
            
            
    
    

