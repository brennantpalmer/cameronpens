<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Orderitem $orderitem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Orderitem'), ['action' => 'edit', $orderitem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Orderitem'), ['action' => 'delete', $orderitem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderitem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Orderitems'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Orderitem'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="orderitems view large-9 medium-8 columns content">
    <h3><?= h($orderitem->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($orderitem->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Currency') ?></th>
            <td><?= h($orderitem->currency) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($orderitem->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Itemid') ?></th>
            <td><?= $this->Number->format($orderitem->itemid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($orderitem->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($orderitem->quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($orderitem->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($orderitem->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($orderitem->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Cptransactionid') ?></th>
                <th scope="col"><?= __('Paypaltransactionid') ?></th>
                <th scope="col"><?= __('Paypalpayerid') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Firstname') ?></th>
                <th scope="col"><?= __('Lastname') ?></th>
                <th scope="col"><?= __('Streetaddress') ?></th>
                <th scope="col"><?= __('Company') ?></th>
                <th scope="col"><?= __('City') ?></th>
                <th scope="col"><?= __('State') ?></th>
                <th scope="col"><?= __('Zip') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Phone') ?></th>
                <th scope="col"><?= __('Tax') ?></th>
                <th scope="col"><?= __('Subtotal') ?></th>
                <th scope="col"><?= __('Total') ?></th>
                <th scope="col"><?= __('Completed') ?></th>
                <th scope="col"><?= __('Cancelled') ?></th>
                <th scope="col"><?= __('Shipped') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($orderitem->orders as $orders): ?>
            <tr>
                <td><?= h($orders->id) ?></td>
                <td><?= h($orders->cptransactionid) ?></td>
                <td><?= h($orders->paypaltransactionid) ?></td>
                <td><?= h($orders->paypalpayerid) ?></td>
                <td><?= h($orders->description) ?></td>
                <td><?= h($orders->firstname) ?></td>
                <td><?= h($orders->lastname) ?></td>
                <td><?= h($orders->streetaddress) ?></td>
                <td><?= h($orders->company) ?></td>
                <td><?= h($orders->city) ?></td>
                <td><?= h($orders->state) ?></td>
                <td><?= h($orders->zip) ?></td>
                <td><?= h($orders->email) ?></td>
                <td><?= h($orders->phone) ?></td>
                <td><?= h($orders->tax) ?></td>
                <td><?= h($orders->subtotal) ?></td>
                <td><?= h($orders->total) ?></td>
                <td><?= h($orders->completed) ?></td>
                <td><?= h($orders->cancelled) ?></td>
                <td><?= h($orders->shipped) ?></td>
                <td><?= h($orders->created) ?></td>
                <td><?= h($orders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
