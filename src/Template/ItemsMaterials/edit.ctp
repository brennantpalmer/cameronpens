<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsMaterial $itemsMaterial
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $itemsMaterial->item_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $itemsMaterial->item_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Items Materials'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="itemsMaterials form large-9 medium-8 columns content">
    <?= $this->Form->create($itemsMaterial) ?>
    <fieldset>
        <legend><?= __('Edit Items Material') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
