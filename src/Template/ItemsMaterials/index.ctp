<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsMaterial[]|\Cake\Collection\CollectionInterface $itemsMaterials
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Items Material'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="itemsMaterials index large-9 medium-8 columns content">
    <h3><?= __('Items Materials') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('item_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('material_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($itemsMaterials as $itemsMaterial): ?>
            <tr>
                <td><?= $itemsMaterial->has('item') ? $this->Html->link($itemsMaterial->item->title, ['controller' => 'Items', 'action' => 'view', $itemsMaterial->item->id]) : '' ?></td>
                <td><?= $itemsMaterial->has('material') ? $this->Html->link($itemsMaterial->material->title, ['controller' => 'Materials', 'action' => 'view', $itemsMaterial->material->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $itemsMaterial->item_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $itemsMaterial->item_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $itemsMaterial->item_id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemsMaterial->item_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
