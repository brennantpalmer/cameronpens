<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ItemsMaterial $itemsMaterial
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Items Material'), ['action' => 'edit', $itemsMaterial->item_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Items Material'), ['action' => 'delete', $itemsMaterial->item_id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemsMaterial->item_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Items Materials'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Items Material'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="itemsMaterials view large-9 medium-8 columns content">
    <h3><?= h($itemsMaterial->item_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Item') ?></th>
            <td><?= $itemsMaterial->has('item') ? $this->Html->link($itemsMaterial->item->title, ['controller' => 'Items', 'action' => 'view', $itemsMaterial->item->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Material') ?></th>
            <td><?= $itemsMaterial->has('material') ? $this->Html->link($itemsMaterial->material->title, ['controller' => 'Materials', 'action' => 'view', $itemsMaterial->material->id]) : '' ?></td>
        </tr>
    </table>
</div>
