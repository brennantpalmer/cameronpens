<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \App\Model\Table\OrderitemsTable|\Cake\ORM\Association\BelongsToMany $Orderitems
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Orderitems', [
            'foreignKey' => 'order_id',
            'targetForeignKey' => 'orderitem_id',
            'joinTable' => 'orders_orderitems'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('cptransactionid')
            ->maxLength('cptransactionid', 255)
            ->requirePresence('cptransactionid', 'create')
            ->notEmpty('cptransactionid');

        $validator
            ->scalar('paypaltransactionid')
            ->maxLength('paypaltransactionid', 255)
            ->allowEmpty('paypaltransactionid');

        $validator
            ->scalar('paypalpayerid')
            ->maxLength('paypalpayerid', 255)
            ->allowEmpty('paypalpayerid');

        $validator
            ->scalar('paypalpaymentid')
            ->maxLength('paypalpaymentid', 255)
            ->allowEmpty('paypalpaymentid');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 128)
            ->allowEmpty('firstname');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 128)
            ->allowEmpty('lastname');

        $validator
            ->scalar('streetaddress')
            ->maxLength('streetaddress', 512)
            ->requirePresence('streetaddress', 'create')
            ->notEmpty('streetaddress');

        $validator
            ->scalar('company')
            ->maxLength('company', 128)
            ->allowEmpty('company');

        $validator
            ->scalar('city')
            ->maxLength('city', 128)
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->scalar('state')
            ->maxLength('state', 128)
            ->requirePresence('state', 'create')
            ->notEmpty('state');

            $validator
            ->scalar('country')
            ->maxLength('country', 128)
            ->requirePresence('country', 'country')
            ->notEmpty('country');

        $validator
            ->scalar('zip')
            ->maxLength('zip', 15)
            ->requirePresence('zip', 'create')
            ->notEmpty('zip');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 32)
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->decimal('tax')
            ->requirePresence('tax', 'create')
            ->notEmpty('tax');

        $validator
            ->decimal('subtotal')
            ->requirePresence('subtotal', 'create')
            ->notEmpty('subtotal');

        $validator
            ->decimal('shipping')
            ->requirePresence('shipping', 'shipping')
            ->notEmpty('shipping');

        $validator
            ->decimal('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->boolean('completed')
            ->allowEmpty('completed');

        $validator
            ->boolean('cancelled')
            ->allowEmpty('cancelled');

        $validator
            ->boolean('shipped')
            ->allowEmpty('shipped');

        $validator
            ->boolean('approved')
            ->allowEmpty('approved');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
