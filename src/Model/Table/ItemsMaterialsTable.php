<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemsMaterials Model
 *
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\BelongsTo $Items
 * @property \App\Model\Table\MaterialsTable|\Cake\ORM\Association\BelongsTo $Materials
 *
 * @method \App\Model\Entity\ItemsMaterial get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemsMaterial newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemsMaterial[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemsMaterial|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemsMaterial patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemsMaterial[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemsMaterial findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemsMaterialsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('items_materials');
        $this->setDisplayField('item_id');
        $this->setPrimaryKey(['item_id', 'material_id']);

        $this->belongsTo('Items', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Materials', [
            'foreignKey' => 'material_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['item_id'], 'Items'));
        $rules->add($rules->existsIn(['material_id'], 'Materials'));

        return $rules;
    }
}
