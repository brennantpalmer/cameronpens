<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @property \App\Model\Table\MaterialsTable|\Cake\ORM\Association\BelongsToMany $Materials
 * @property \App\Model\Table\PhotosTable|\Cake\ORM\Association\BelongsToMany $Photos
 * @property \App\Model\Table\TagsTable|\Cake\ORM\Association\BelongsToMany $Tags
 *
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ItemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('items');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Materials', [
            'foreignKey' => 'item_id',
            'targetForeignKey' => 'material_id',
            'joinTable' => 'items_materials'
        ]);
        $this->belongsToMany('Photos', [
            'foreignKey' => 'item_id',
            'targetForeignKey' => 'photo_id',
            'joinTable' => 'items_photos'
        ]);
        $this->belongsToMany('Tags', [
            'foreignKey' => 'item_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'items_tags'
        ]);
        $this->belongsToMany('Categories', [
            'foreignKey' => 'item_id',
            'targetForeignKey' => 'category_id',
            'joinTable' => 'items_categories'
        ]);

        // Add the behaviour to the table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->value('item_id')
            
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
            ->add('q', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['title', 'description']
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 191)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug')
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->boolean('onsale')
            ->allowEmpty('onsale');

        $validator
            ->boolean('active')
            ->allowEmpty('active');

        $validator
            ->boolean('featured')
            ->allowEmpty('featured');

        $validator
            ->boolean('hot')
            ->allowEmpty('hot');

        $validator
            ->boolean('favorite')
            ->allowEmpty('favorite');

        $validator
            ->boolean('bundle')
            ->allowEmpty('bundle');

        $validator
            ->decimal('standardprice')
            ->allowEmpty('standardprice');

        $validator
            ->decimal('saleprice')
            ->allowEmpty('saleprice');

        $validator
            ->decimal('actualprice')
            ->allowEmpty('actualprice');

        $validator
            ->integer('productiontime')
            ->allowEmpty('productiontime');

        $validator
            ->integer('numberinstock')
            ->allowEmpty('numberinstock');

            

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['slug']));

        return $rules;
    }
}
