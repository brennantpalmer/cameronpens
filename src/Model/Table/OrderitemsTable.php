<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orderitems Model
 *
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\BelongsToMany $Orders
 *
 * @method \App\Model\Entity\Orderitem get($primaryKey, $options = [])
 * @method \App\Model\Entity\Orderitem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Orderitem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Orderitem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Orderitem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Orderitem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Orderitem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrderitemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orderitems');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Orders', [
            'foreignKey' => 'orderitem_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'orders_orderitems'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('itemid')
            ->requirePresence('itemid', 'create')
            ->notEmpty('itemid');

        $validator
            ->scalar('title')
            ->maxLength('title', 256)
            ->allowEmpty('title');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->scalar('currency')
            ->maxLength('currency', 16)
            ->requirePresence('currency', 'create')
            ->notEmpty('currency');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
