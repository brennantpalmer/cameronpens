<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdersOrderitems Model
 *
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\BelongsTo $Orders
 * @property \App\Model\Table\OrderitemsTable|\Cake\ORM\Association\BelongsTo $Orderitems
 *
 * @method \App\Model\Entity\OrdersOrderitem get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrdersOrderitem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrdersOrderitem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrdersOrderitem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrdersOrderitem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersOrderitem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersOrderitem findOrCreate($search, callable $callback = null, $options = [])
 */
class OrdersOrderitemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders_orderitems');
        $this->setDisplayField('order_id');
        $this->setPrimaryKey(['order_id', 'orderitem_id']);

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orderitems', [
            'foreignKey' => 'orderitem_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['orderitem_id'], 'Orderitems'));

        return $rules;
    }
}
