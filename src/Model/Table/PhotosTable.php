<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Photos Model
 *
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\BelongsToMany $Items
 *
 * @method \App\Model\Entity\Photo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Photo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Photo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Photo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Photo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Photo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Photo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PhotosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('photos');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'title' => [
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // Store the thumbnail in a temporary file
                    $browseThumbnail = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

                    // Image size for homepage and browse pages
                    $size = new \Imagine\Image\Box(450, 600);
                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                    $imagine = new \Imagine\Gd\Imagine();

                    // Save that modified file to our temp file
                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size, $mode)
                        ->save($browseThumbnail);

                    // Store the thumbnail in a temporary file
                    $detailSmallTumbnail = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;    
                    
                    // Image size for small pictures on the view item page
                    $size = new \Imagine\Image\Box(500, 500);
                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                    $imagine = new \Imagine\Gd\Imagine();

                    // Save that modified file to our temp file
                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size, $mode)
                        ->save($detailSmallTumbnail);

                    // Store the thumbnail in a temporary file
                    $detailBigThumbnail = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

                    // Image size for homepage and browse pages
                    $size = new \Imagine\Image\Box(450, 600);
                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                    $imagine = new \Imagine\Gd\Imagine();

                    // Save that modified file to our temp file
                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size, $mode)
                        ->save($detailBigThumbnail);    
                    // Now return the original *and* the thumbnails
                    return [
                        $data['tmp_name'] => $data['name'],
                        $browseThumbnail => 'browse-' . $data['name'],
                        $detailSmallTumbnail => 'detailsmall-'  . $data['name'],
                        $detailBigThumbnail => 'detailbig-'  . $data['name'],
                    ];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        $path . 'browse-' . $entity->{$field},
                        $path . 'detailsmall-' . $entity->{$field},
                        $path . 'detailbig-' . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ]
        ]);

        $this->belongsToMany('Items', [
            'foreignKey' => 'photo_id',
            'targetForeignKey' => 'item_id',
            'joinTable' => 'items_photos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        return $validator;
    }
}
