<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Config Entity
 *
 * @property int $id
 * @property float $standardshipping
 * @property float $expressshipping
 * @property float $twodayshipping
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Config extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'standardshipping' => true,
        'expressshipping' => true,
        'twodayshipping' => true,
        'tax' => true,
        'paypalsecretkey' => true,
        'paypalclientid' => true,
        'created' => true,
        'modified' => true,
        'maintenancemode' => true
    ];
}
