<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property bool $onsale
 * @property bool $active
 * @property bool $featured
 * @property float $standardprice
 * @property float $saleprice
 * @property int $productiontime
 * @property int $numberinstock
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Material[] $materials
 * @property \App\Model\Entity\Photo[] $photos
 * @property \App\Model\Entity\Tag[] $tags
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'slug' => true,
        'description' => true,
        'onsale' => true,
        'active' => true,
        'featured' => true,
        'standardprice' => true,
        'saleprice' => true,
        'productiontime' => true,
        'numberinstock' => true,
        'created' => true,
        'modified' => true,
        'actualprice' => true,
        'materials' => true,
        'photos' => true,
        'tags' => true,
        'categories' => true,
        'favorite' => true,
        'hot' => true,
        'bundle' => true
    ];
}
