<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property string $cptransactionid
 * @property string $paypaltransactionid
 * @property string $paypalpayerid
 * @property string $description
 * @property string $firstname
 * @property string $lastname
 * @property string $streetaddress
 * @property string $company
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $email
 * @property string $phone
 * @property float $tax
 * @property float $subtotal
 * @property float $total
 * @property bool $completed
 * @property bool $cancelled
 * @property bool $shipped
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Orderitem[] $orderitems
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cptransactionid' => true,
        'paypaltransactionid' => true,
        'paypalpayerid' => true,
        'description' => true,
        'firstname' => true,
        'lastname' => true,
        'streetaddress' => true,
        'company' => true,
        'city' => true,
        'state' => true,
        'zip' => true,
        'email' => true,
        'phone' => true,
        'tax' => true,
        'subtotal' => true,
        'total' => true,
        'completed' => true,
        'cancelled' => true,
        'shipped' => true,
        'created' => true,
        'modified' => true,
        'orderitems' => true,
        'country' => true,
        'shipping' => true,
        'paypalpaymentid' => true,
        'approved' => true
    ];
}
