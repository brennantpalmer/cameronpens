<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Orderitem Entity
 *
 * @property int $id
 * @property int $itemid
 * @property string $title
 * @property float $price
 * @property string $currency
 * @property int $quantity
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Order[] $orders
 */
class Orderitem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'itemid' => true,
        'title' => true,
        'price' => true,
        'currency' => true,
        'quantity' => true,
        'created' => true,
        'modified' => true,
        'orders' => true
    ];
}
